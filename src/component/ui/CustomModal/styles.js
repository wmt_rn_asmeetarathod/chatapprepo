import {StyleSheet} from 'react-native';
import {ThemeUtils, Color, IS_IOS} from 'src/utils';

export default StyleSheet.create({
    // Modal styles
    modalContainer: {
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignSelf: 'center',
        width: ThemeUtils.relativeWidth(90),
    },
    modalUpperView: {
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    modalView: {
        margin: 30,
        backgroundColor: Color.BLACK,
        borderRadius: 20,
        borderColor: Color.TEXT_PRIMARY,
        borderWidth: 0.5,
        padding: 25,
        justifyContent: 'center',
        alignSelf: 'center',
        width: ThemeUtils.relativeWidth(90),
        shadowColor: Color.WHITE, //PRIMARY
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 3,
    },
    vwButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    outlinedLabelStyle: {
        borderRadius: 4,
        backgroundColor: 'transparent',
        borderWidth: 1,
        borderColor: Color.ACCENT_COLOR,
        marginBottom: 10,
        color: Color.WHITE,
        fontFamily: ThemeUtils.FontStyle.Lato_regular,
        padding: 10,
        textAlignVertical: 'top',
        maxHeight: ThemeUtils.relativeHeight(16),
        minHeight: IS_IOS
            ? ThemeUtils.relativeHeight(10)
            : ThemeUtils.relativeHeight(1),
    },
    errorText: {
        fontSize: ThemeUtils.fontXSmall,
        marginVertical: 5,
    },
});
