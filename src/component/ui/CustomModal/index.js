import React from 'react';
import {View, Modal} from 'react-native';
import {Controller, useForm} from 'react-hook-form';

// custome component
import {Label, AppButton} from 'src/component';

// utils
import {CommonStyle, ThemeUtils, Color, IS_IOS} from 'src/utils';

//third-party
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';

import styles from './styles';

const CustomModal = (props) => {
    const {
        modalVisible,
        input,
        labels,
        label1,
        label2,
        button1Text,
        button2Text,
        button1Press,
        button2Press,
        onRequestClose,
    } = props;

    const {
        handleSubmit,
        control,
        clearErrors,
        setValue,
        formState: {errors},
    } = useForm();

    return (
        <View style={styles.modalContainer}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => onRequestClose()}>
                <View style={styles.modalUpperView}>
                    <View style={styles.modalView}>
                        {input && (
                            <Controller
                                control={control}
                                render={({
                                    field: {onChange, onBlur, value},
                                }) => (
                                    <>
                                        <Label
                                            fontfamily={
                                                ThemeUtils.FontStyle
                                                    .Lato_regular
                                            }
                                            small
                                            mb={10}
                                            children={label1}
                                        />
                                        <AutoGrowingTextInput
                                            style={styles.outlinedLabelStyle}
                                            numberOfLines={IS_IOS ? 5 : 5}
                                            multiline
                                            onBlur={onBlur}
                                            onFocus={() =>
                                                clearErrors('report')
                                            }
                                            error={errors.report?.message}
                                            value={value}
                                            onChangeText={onChange}
                                            returnKeyType="done"
                                        />
                                        {errors.report?.message && (
                                            <Label
                                                children={
                                                    errors.report?.message
                                                }
                                                fontfamily={
                                                    ThemeUtils.FontStyle
                                                        .Lato_regular
                                                }
                                                color={Color.ERROR_COLOR}
                                                style={styles.errorText}
                                            />
                                        )}
                                    </>
                                )}
                                name={'report'}
                                defaultValue={''}
                                rules={{
                                    required: 'Report is Required',
                                }}
                            />
                        )}
                        {labels && (
                            <>
                                <Label
                                    children={label1}
                                    color={Color.WHITE}
                                    mb={15}
                                />
                                <Label
                                    children={label2}
                                    color={Color.TEXT_PLACEHOLDER}
                                    mb={5}
                                />
                            </>
                        )}
                        <View style={styles.vwButtonContainer}>
                            <AppButton
                                click={() => {
                                    setValue('report', null);
                                    button1Press();
                                }}
                                width={ThemeUtils.relativeWidth(32)}
                                backgroundColor={Color.BLACK}
                                btn_sm
                                mt={ThemeUtils.relativeRealHeight(3)}
                                style={CommonStyle.full_flex}>
                                {button1Text}
                            </AppButton>
                            <AppButton
                                click={
                                    labels
                                        ? () => button2Press()
                                        : handleSubmit(button2Press)
                                }
                                width={ThemeUtils.relativeWidth(32)}
                                backgroundColor={Color.ACCENT_COLOR}
                                textColor={Color.BLACK}
                                btn_sm
                                mt={ThemeUtils.relativeRealHeight(3)}
                                style={CommonStyle.full_flex}>
                                {button2Text}
                            </AppButton>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

export default CustomModal;
