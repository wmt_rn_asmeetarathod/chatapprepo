import React from 'react';

import {View, Image, TouchableOpacity} from 'react-native';

import {styles} from './styles';

// Custom Component
import {Label} from 'src/component';

// images
import GROUP_PLACEHOLDER from 'src/assets/images/group_placeholder.png';

// Utils
import {Color, ThemeUtils} from 'src/utils';

// third-party
import moment from 'moment';
import Svg, {Circle} from 'react-native-svg';

const StatusContainer = (props) => {
    const {seen} = props;
    const {username, stories} = props?.item;

    return (
        <TouchableOpacity
            onPress={() => props.onStatusPress(props.index)}
            style={styles.container}>
            <Svg
                width={ThemeUtils.relativeWidth(17)}
                height={ThemeUtils.relativeWidth(17)}
                viewBox="0 0 100 100">
                <Circle
                    cx="50"
                    cy="50"
                    r="48"
                    fill="none"
                    stroke={seen ? Color.TEXT_PLACEHOLDER : Color.PRIMARY}
                    strokeWidth="4"
                    // strokeDasharray={`${360 / 10} 4`}
                    strokeDasharray={'360 4'}
                    strokeDashoffset="0"
                    // strokeDashoffset={(2 * 3.14 * 100) / 4}
                />
                <Image
                    source={
                        stories[0]?.url
                            ? {
                                  uri: stories[0]?.url,
                              }
                            : GROUP_PLACEHOLDER
                    }
                    style={styles.imageStyle}
                />
            </Svg>
            <View style={styles.vwLabelStyle}>
                <Label bolder>{username}</Label>
                <Label small>
                    {moment(stories[0]?.created).calendar().replace(' at', ',')}
                </Label>
            </View>
        </TouchableOpacity>
    );
};

export default StatusContainer;
