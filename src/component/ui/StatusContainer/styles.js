import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        // flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: Color.WHITE,
        padding: 10,
        // elevation: 10,
    },
    vwImage: {
        height: ThemeUtils.relativeWidth(16),
        width: ThemeUtils.relativeWidth(16),
        borderRadius: ThemeUtils.relativeWidth(8),
        borderColor: Color.PRIMARY,
        borderWidth: 2,
    },
    imageStyle: {
        height: ThemeUtils.relativeWidth(15),
        width: ThemeUtils.relativeWidth(15),
        borderRadius: ThemeUtils.relativeWidth(7.5),
        borderColor: Color.WHITE,
        borderWidth: 3,
        marginTop: 3.5,
        marginStart: 3.5,
    },
    vwLabelStyle: {
        marginStart: 15,
    },
});
