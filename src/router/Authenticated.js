import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Routes from 'src/router/Routes';

// Screens Name
import Chat from 'src/screens/Authenticated/ChatScreen';
import Profile from 'src/screens/Authenticated/Profile';
import ChangePassword from 'src/screens/Authenticated/ChangePassword';
import CreateGroup from 'src/screens/Authenticated/CreateGroup';
import GroupChat from 'src/screens/Authenticated/GroupChat';
import GroupDetails from 'src/screens/Authenticated/GroupDetails';
import GroupName from 'src/screens/Authenticated/GroupName';
import GroupImage from 'src/screens/Authenticated/GroupImage';
import UserDetails from 'src/screens/Authenticated/UserDetails';
import DisplayImage from 'src/screens/Authenticated/DisplayImage';
import DisplayPdf from 'src/screens/Authenticated/DisplayPdf';
import CaptureStatus from 'src/screens/Authenticated/CaptureStatus';
import Main from 'src/screens/Authenticated/Main';

const Stack = createStackNavigator();

const navigator = () => {
    return (
        <Stack.Navigator
            initialRouteName={Routes.Main}
            screenOptions={({navigation}) => ({
                headerTitleAlign: 'center',
            })}>
            <Stack.Screen
                name={Routes.Main}
                component={Main}
                options={{
                    headerShown: true,
                    headerTitleAlign: 'center',
                }}
            />
            <Stack.Screen
                name={Routes.Chat}
                component={Chat}
                options={{title: ''}}
            />
            <Stack.Screen
                name={Routes.Profile}
                component={Profile}
                options={{title: 'My Profile'}}
            />
            <Stack.Screen
                name={Routes.ChangePassword}
                component={ChangePassword}
                options={{title: 'Change Password'}}
            />
            <Stack.Screen
                name={Routes.CreateGroup}
                component={CreateGroup}
                options={{title: 'Create New Group'}}
            />
            <Stack.Screen
                name={Routes.GroupChat}
                component={GroupChat}
                options={{title: ''}}
            />
            <Stack.Screen
                name={Routes.GroupDetails}
                component={GroupDetails}
                options={{title: 'Group Details'}}
            />
            <Stack.Screen
                name={Routes.GroupName}
                component={GroupName}
                options={{title: 'Edit New Subject'}}
            />
            <Stack.Screen
                name={Routes.GroupImage}
                component={GroupImage}
                options={{title: 'Group Icon'}}
            />
            <Stack.Screen
                name={Routes.UserDetails}
                component={UserDetails}
                options={{title: 'User Details'}}
            />
            <Stack.Screen
                name={Routes.DisplayImage}
                component={DisplayImage}
                options={{title: 'Chats Image'}}
            />
            <Stack.Screen
                name={Routes.DisplayPdf}
                component={DisplayPdf}
                options={{title: 'Chats Pdf'}}
            />
            <Stack.Screen
                name={Routes.CaptureStatus}
                component={CaptureStatus}
                options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default navigator;
