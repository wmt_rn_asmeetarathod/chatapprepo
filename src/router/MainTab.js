import React from 'react';

// screens
import Home from 'src/screens/Authenticated/Home';
import Status from 'src/screens/Authenticated/Status';

import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

//navigation
import Routes from './Routes';

const Tab = createMaterialTopTabNavigator();

const MainTab = () => (
    <Tab.Navigator>
        <Tab.Screen name={Routes.Home} component={Home} />
        <Tab.Screen name={Routes.Status} component={Status} />
    </Tab.Navigator>
);

export default MainTab;
