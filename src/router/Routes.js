const Routes = {
    /*  Root Stacks    */

    Authenticated: 'Authenticated',
    UnAuthenticated: 'UnAuthenticated',
    Splash: 'Splash',

    /*  Non-Authenticated Routes    */
    Login: 'Login',
    SignUp: 'Signup',

    /*  Authenticated Routes    */

    Home: 'Home',
    Chat: 'Chat',
    Profile: 'Profile',
    ChangePassword: 'ChangePassword',
    CreateGroup: 'CreateGroup',
    GroupChat: 'GroupChat',
    GroupDetails: 'GroupDetails',
    GroupName: 'GroupName',
    GroupImage: 'GroupImage',
    UserDetails: 'UserDetails',
    DisplayImage: 'DisplayImage',
    DisplayPdf: 'DisplayPdf',
    Status: 'Status',
    MainTab: 'MainTab',
    CaptureStatus: 'CaptureStatus',
    Main: 'Main',
};
export default Routes;
