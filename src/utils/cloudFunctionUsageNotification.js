/* eslint-disable  */

/*  this file is added just for code reference of push notification using Cloud Function  */

exports.sendNotification = functions.database.ref('/notificationRequests/{values}').onCreate((event) => {

    // Grab the current value of what was written to the Realtime Database.
    var itemVal = event.data.val()

    // Notification details.
    const payload = {
        notification: {
            title: itemVal.title,
            body: itemVal.body,
            sound: 'default',

        },
        data: {
            otherData: "extra"
        },
    };

    // Set the message as high priority and have it expire after 24 hours.
    const options = {
        contentAvailable: true,
        priority: 'high',
        timeToLive: 60 * 60 * 24,
    };

    if (itemVal.tokens) {
        // Send a message to the device corresponding to the provided
        // registration token with the provided options.
        return admin.messaging().sendToDevice(itemVal.tokens, payload, options)
            .then((response) => {


            })
            .catch((error) => {
                console.log('Error : ', error);

            });
    } else {

    }
});