// api response codes
export const ResponseCode = {
    OK: 200,
    CREATED: 201,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    METHOD_NOT_ALLOWED: 405,
    UNPROCESSABLE_REQUEST: 422,
    INTERNAL_SERVER_ERROR: 500,
    TOKEN_INVALID: 503,
    NO_INTERNET: 522,
    BAD_GATEWAY: 502,
};

const Regex = {
    PASSWORD:
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
};

const MessageType = {
    SUCCESS: 1,
    FAILED: 2,
    INFO: 3,
};

const STRIPE_API_KEYS = {
    PUBLISHABLE_API_KEY:
        'pk_test_51Jolg5SCS1HgBlUbxYWtyzWfN9FWJ9hiqY63EyVOgXHHnPeDU7yZxEoR1iIMyhPN6iitWdT7m8QYIAwO1u9REXlk00M1PKMS1p',
    SECRET_API_KET:
        'sk_test_51Jolg5SCS1HgBlUb2ChkKUQMIT0uVHtSHkYlOgyDju7jTeBOGe9nPOHtfQT0mZDTSa65fZcNcIQcUxnN7Z0Wrdt800bepSDTmh',
};

const WebClient = {
    KEY: '964615024049-0c1400m6mamnn5juonk1hop4lq90890o.apps.googleusercontent.com',
    SECRET: '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',
};

export default {
    Regex,
    ResponseCode,
    MessageType,
    STRIPE_API_KEYS,
    WebClient,
};
