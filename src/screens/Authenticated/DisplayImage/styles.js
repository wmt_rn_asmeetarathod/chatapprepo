import {StyleSheet} from 'react-native';
import {ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },
    imageStyle: {
        height: ThemeUtils.relativeWidth(100),
        width: ThemeUtils.relativeWidth(100),
        resizeMode: 'cover',
    },
});
