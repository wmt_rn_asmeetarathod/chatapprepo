import React from 'react';

import {View, Image} from 'react-native';

import {styles} from './styles';

const DisplayImage = (props) => {
    const {uri} = props?.route?.params;
    return (
        <View style={styles.container}>
            <Image source={{uri: uri}} style={styles.imageStyle} />
        </View>
    );
};

export default DisplayImage;
