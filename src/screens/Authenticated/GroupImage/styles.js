import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    groupImageStyle: {
        height: ThemeUtils.relativeWidth(100),
        width: ThemeUtils.relativeWidth(100),
        resizeMode: 'cover',
        justifyContent: 'flex-end',
    },
    editIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.WHITE,
        borderRadius: ThemeUtils.relativeWidth(10),
        padding: 7,
        marginEnd: 10,
    },
    editIcon: {},
    vwModal: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Color.PRIMARY_DARK,
        alignItems: 'flex-start',
        justifyContent: 'center',
        height: ThemeUtils.relativeHeight(18),
        width: ThemeUtils.relativeWidth(100),
        padding: 10,
    },
    vwChooser: {
        flexDirection: 'row',
    },
    chooseCameraIconStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.WHITE,
        borderRadius: ThemeUtils.relativeHeight(5),
        padding: 12,
    },
    vwSelector: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
    },
});
