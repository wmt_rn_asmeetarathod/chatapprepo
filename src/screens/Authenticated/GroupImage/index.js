/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useLayoutEffect} from 'react';
import {View, Modal, Image} from 'react-native';
import {styles} from './styles';

// Custom Component
import {Label, Ripple} from 'src/component';

// images
import GROUP_PLACEHOLDER from 'src/assets/images/group_placeholder.png';

// Utils
import {
    Strings,
    Color,
    ThemeUtils,
    PermissionUtils,
    showToast,
    ToastType,
} from 'src/utils';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import storage from '@react-native-firebase/storage';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const GroupImage = (props) => {
    const {chatKey} = props.route?.params;
    const userId = auth().currentUser.uid;
    const [visible, setVisible] = useState(false);
    const [imageUri, setImageUri] = useState(null);
    const [participants, setParticipants] = useState([]);

    /*  Life-cycles Methods */

    useEffect(async () => {
        await storage()
            .ref(chatKey)
            .getDownloadURL()
            .then((url) => {
                setImageUri(url);
            });

        database()
            .ref(`groups/${userId}/${chatKey}`)
            .once('value')
            .then((snapshot) => {
                setParticipants(snapshot.val().participants);
            });
        return () => {};
    }, []);

    useLayoutEffect(() => {
        props.navigation.setOptions({
            headerRight: () => {
                return (
                    <>
                        <Ripple
                            rippleContainerBorderRadius={50}
                            onPress={() => handleEditGroupImage()}
                            style={styles.editIconStyle}>
                            <IonIcon
                                name="pencil"
                                style={styles.editIcon}
                                color={Color.PRIMARY_DARK}
                                size={ThemeUtils.relativeWidth(5)}
                            />
                        </Ripple>
                    </>
                );
            },
        });
    }, []);

    /* UI-Events Methods */

    const updateImage = (imageUrl) => {
        const userRef = storage().ref(chatKey);

        userRef.putFile(imageUrl.toString()).on('state_changed', (snapshot) => {
            switch (snapshot.state) {
                case 'success':
                    userRef.getDownloadURL().then((url) => {
                        setImageUri(url);
                        database().ref(`groups/${userId}/${chatKey}`).update({
                            groupImageUrl: url,
                        });
                        participants?.map((item) => {
                            database()
                                .ref(`groups/${item?.uid}/${chatKey}`)
                                .update({
                                    groupImageUrl: url,
                                });
                        });
                    });
                    showToast(
                        'Success',
                        Strings.updateSuccess,
                        ToastType.SUCCESS,
                    );
                    break;

                default:
                    break;
            }
        });
    };

    const handleEditGroupImage = () => {
        setVisible(true);
    };

    const handleRemovePhoto = () => {
        setVisible(false);
        const userRef = storage().ref(chatKey);

        userRef.delete().then(() => {
            database().ref(`groups/${userId}/${chatKey}`).update({
                groupImageUrl: null,
            });
            participants?.map((item) => {
                database().ref(`groups/${item?.uid}/${chatKey}`).update({
                    groupImageUrl: null,
                });
            });
            setImageUri(null);
            showToast('Success', Strings.removeSuccess, ToastType.SUCCESS);
        });
    };

    const handleCapturePhoto = () => {
        setVisible(false);

        PermissionUtils.requestCameraPermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openCamera();
            }
        });
    };

    const handleChoosePhoto = () => {
        setVisible(false);

        /* For Storage Permission */
        PermissionUtils.requestStoragePermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openGallary();
            }
        });
    };

    const toggleBottomNavigationView = () => {
        setVisible(!visible);
    };

    const openCamera = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
            saveToPhotos: true,
        };
        launchCamera(options, (response) => {
            if (response.didCancel) {
                setImageUri(imageUri);
            } else if (response.errorCode == 'camera_unavailable') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'permission') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'others') {
                setImageUri(imageUri);
            } else {
                // setImageUri(imageUri);
                updateImage(response.uri);
            }
        });
    };

    const openGallary = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
        };
        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                setImageUri(imageUri);
            } else if (response.errorCode == 'camera_unavailable') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'permission') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'others') {
                setImageUri(imageUri);
            } else {
                // setImageUri(imageUri);
                updateImage(response.uri);
            }
        });
    };

    return (
        <View style={styles.container}>
            <Image
                source={
                    imageUri
                        ? {
                              uri: imageUri,
                          }
                        : GROUP_PLACEHOLDER
                }
                style={styles.groupImageStyle}
            />
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
                onRequestClose={() => toggleBottomNavigationView()}>
                <View style={styles.vwModal}>
                    <Label bolder color={Color.WHITE} mb={5}>
                        {Strings.groupIcon}
                    </Label>
                    <View style={styles.vwChooser}>
                        <View style={styles.vwSelector}>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleRemovePhoto()}
                                style={styles.chooseCameraIconStyle}>
                                <IonIcon
                                    name="trash"
                                    style={styles.searchIcon}
                                    color={Color.PRIMARY_DARK}
                                    size={ThemeUtils.relativeWidth(10)}
                                />
                            </Ripple>
                            <Label xsmall color={Color.WHITE}>
                                {Strings.remove}
                            </Label>
                        </View>
                        <View style={styles.vwSelector}>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleCapturePhoto()}
                                style={styles.chooseCameraIconStyle}>
                                <View>
                                    <IonIcon
                                        name="camera"
                                        style={styles.searchIcon}
                                        color={Color.PRIMARY_DARK}
                                        size={ThemeUtils.relativeWidth(10)}
                                    />
                                </View>
                            </Ripple>
                            <Label xsmall color={Color.WHITE}>
                                {Strings.camera}
                            </Label>
                        </View>
                        <View style={styles.vwSelector}>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleChoosePhoto()}
                                style={styles.chooseCameraIconStyle}>
                                <IonIcon
                                    name="image"
                                    style={styles.searchIcon}
                                    color={Color.PRIMARY_DARK}
                                    size={ThemeUtils.relativeWidth(10)}
                                />
                            </Ripple>
                            <Label xsmall color={Color.WHITE}>
                                {Strings.gallery}
                            </Label>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

export default GroupImage;
