/* eslint-disable  */

import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'center',
    },
    flatListStyle: {
        flex: 1,
    },
    myStyle: {
        backgroundColor: Color.PRIMARY_DARK,
        color: Color.WHITE,
        alignSelf: 'flex-end',
        paddingVertical: 10,
        elevation: 5,
        borderTopLeftRadius: 17,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 5,
        paddingHorizontal: 33,
    },
    myImageStyle: {
        alignSelf: 'flex-end',
        elevation: 5,
        borderTopLeftRadius: 17,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 5,
    },
    otherStyle: {
        backgroundColor: Color.MANDARIAN_ORANGE,
        color: Color.WHITE,
        borderRadius: 30,
        alignSelf: 'flex-start',
        paddingVertical: 10,
        elevation: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 17,
        paddingHorizontal: 33,

        // maxWidth: ThemeUtils.relativeWidth(85),
        // alignItems: 'center',
    },
    otherImageStyle: {
        alignSelf: 'flex-start',
        elevation: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 17,
    },
    myTimeStyle: {
        alignSelf: 'flex-end',
    },
    otherTimeStyle: {
        alignSelf: 'flex-start',
    },
    myChatImageStyle: {
        height: ThemeUtils.relativeWidth(60),
        width: ThemeUtils.relativeWidth(60),
        borderWidth: 6,
        borderTopLeftRadius: 17,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 5,
    },
    otherChatImageStyle: {
        height: ThemeUtils.relativeWidth(60),
        width: ThemeUtils.relativeWidth(60),
        borderWidth: 6,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 17,
    },
    itemStyle: {
        padding: ThemeUtils.relativeRealHeight(0.5),
    },
    sendContainerStyle: {
        position: 'absolute',
        bottom: 0,
        left: 10,
        right: 10,
        maxHeight: ThemeUtils.relativeRealHeight(12),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 7,
    },
    contentStyle: {
        flex: 1,
        color: Color.WHITE,
        paddingStart: 20,
        paddingEnd: 45,
        backgroundColor: Color.PRIMARY_DARK,
        borderRadius: 30,
        maxHeight: ThemeUtils.relativeRealHeight(12),
        marginVertical: 5,
    },
    sendLabelStylewithColor: {
        color: Color.PRIMARY_DARK,
        alignSelf: 'center',
        marginStart: 5,
    },
    sendLabelStylewithoutColor: {
        color: Color.LIGHT_GRAY,
        alignSelf: 'center',
        marginStart: 5,
    },
    dayStyle: {
        alignItems: 'center',
        marginVertical: ThemeUtils.relativeRealHeight(2),
    },
    headerLeftStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    userProfileStyle: {
        width: ThemeUtils.relativeWidth(10),
        height: ThemeUtils.relativeWidth(10),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(5),
        // marginEnd : 20,
    },
    headerStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    receiverNameStyle: {
        width: ThemeUtils.relativeWidth(68),
    },
    attachIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 55,
    },
    attachIcon: {
        transform: [
            {
                rotateZ: '320deg',
            },
        ],
    },
    vwModal: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Color.MODAL_BACKGROUND_COLOR,
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        paddingTop: 10,
    },
    vwChooser: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'flex-start',
    },
    chooseCameraIconStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: Color.WHITE,
        borderRadius: ThemeUtils.relativeHeight(5),
        padding: 20,
    },
    playIconStyle: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    vwSelector: {
        justifyContent: 'center',
        alignItems: 'center',
        height: ThemeUtils.relativeRealWidth(28),
        width: ThemeUtils.relativeRealWidth(100) / 3,
    },
    contentContainerStyle: {},
    documentBackground: {
        backgroundColor: Color.DOCUMENT,
    },
    cameraBackground: {
        backgroundColor: Color.CAMERA,
    },
    galleryBackground: {
        backgroundColor: Color.GALLERY,
    },
    audioBackground: {
        backgroundColor: Color.AUDIO,
    },
    paymentBackground: {
        backgroundColor: Color.PAYMENT,
    },
    roomBackground: {
        backgroundColor: Color.ROOM,
    },
    locationBackground: {
        backgroundColor: Color.LOCATION,
    },
    contactBackground: {
        backgroundColor: Color.CONTACT,
    },
    chatInnerCont: {
        flexDirection: 'row',
        alignItems: 'center',
        maxWidth: ThemeUtils.relativeWidth(88.5),
    },
});
