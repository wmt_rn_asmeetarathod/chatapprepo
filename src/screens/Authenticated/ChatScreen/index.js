/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useRef, useLayoutEffect} from 'react';
import {CommonActions} from '@react-navigation/routers';
import {HeaderBackButton} from '@react-navigation/stack';

import {
    FlatList,
    ToastAndroid,
    View,
    Image,
    Modal,
    ScrollView,
    TouchableOpacity,
} from 'react-native';

import {styles} from './styles';

// Custom Component
import {Label, Ripple, AppButton, PaymentView} from 'src/component';

// Utils
import {
    CommonStyle,
    Strings,
    ThemeUtils,
    Color,
    PermissionUtils,
    IS_IOS,
    Constants,
} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import moment from 'moment';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import {selectContactPhone} from 'react-native-select-contact';
import SoundPlayer from 'react-native-sound-player';
import stripe from 'tipsi-stripe';
import {
    CardField,
    StripeProvider,
    useStripe,
} from '@stripe/stripe-react-native';
import axios from 'axios';
// import Stripe from 'stripe';
// import Meeting from 'google-meet-api';

const Chat = (props) => {
    const otherUid = props.route.params.receiverId;
    const userId = props.route.params.userId;
    const user = auth().currentUser;
    const [chats, setChats] = useState([]);
    const [content, setContent] = useState('');
    const chatKey = database().ref(`users/${userId}/${otherUid}`).push().key;
    const listRef = useRef();
    const [senderName, setSenderName] = useState();
    const [receiverName, setReceiverName] = useState();
    const [sendContainerHeight, setSendContainerHeight] = useState(
        ThemeUtils.relativeRealHeight(8.5),
    );
    const [containsText, setContainsText] = useState(false);
    const [imageUri, setImageUri] = useState();
    const [visible, setVisible] = useState(false);
    const [paymentVisible, setPaymentVisible] = useState(false);
    const [response, setResponse] = useState();
    const [paymentStatus, setPaymentStatus] = useState('');
    const [isPlaying, setIsPlaying] = useState(null);
    const [chatId, setChatId] = useState(0);
    const sharingOptions = [
        {
            id: 1,
            style: styles.documentBackground,
            iconName: 'document',
            title: Strings.document,
            type: 'DOCUMENT',
        },
        {
            id: 2,
            style: styles.cameraBackground,
            iconName: 'camera',
            title: Strings.camera,
            type: 'CAMERA',
        },
        {
            id: 3,
            style: styles.galleryBackground,
            iconName: 'image',
            title: Strings.gallery,
            type: 'GALLERY',
        },
        {
            id: 4,
            style: styles.audioBackground,
            iconName: 'headset',
            title: Strings.audio,
            type: 'AUDIO',
        },
        {
            id: 5,
            style: styles.paymentBackground,
            iconName: 'logo-usd',
            title: Strings.payment,
            type: 'PAYMENT',
        },
        {
            id: 6,
            style: styles.locationBackground,
            iconName: 'location',
            title: Strings.location,
            type: 'LOCATION',
        },
        {
            id: 7,
            style: styles.contactBackground,
            iconName: 'person',
            title: Strings.contact,
            type: 'CONTACT',
        },
    ];
    const cartInfo = {
        id: '5eruyt35eggr76476236523t3',
        description: 'T Shirt - With react Native Logo',
        amount: 1,
    };

    /*  Life-cycles Methods */

    useEffect(() => {
        return () => {
            setIsPlaying(null);
        };
    }, []);

    useEffect(() => {}, [senderName, receiverName, paymentVisible]);

    useEffect(() => {}, [chats]);

    useEffect(() => {
        stripe.setOptions({
            publishableKey: Constants.STRIPE_API_KEYS.PUBLISHABLE_API_KEY,
        });

        return () => {};
    }, []);

    useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: (props) => {
                return (
                    <View style={styles.headerLeftStyle}>
                        <HeaderBackButton {...props} />
                        {renderProfileImage()}
                    </View>
                );
            },
        });
    }, [receiverName, imageUri]);

    useEffect(() => {
        let mounted = true;
        if (user) {
            try {
                // retrieve chats from node
                database()
                    .ref(`users/${userId}`)
                    .on('value', (snap) => {
                        if (snap.hasChild(`${otherUid}`)) {
                            let temp = snap.val();
                            let key = temp[otherUid].chatKey;
                            if (mounted) {
                                database()
                                    .ref(`chats/${key}`)
                                    .on('value', (snapshot) => {
                                        let chats = [];
                                        snapshot.forEach((snap) => {
                                            chats.push(snap.val());
                                        });
                                        setChats(chats);
                                        generateGroupedChat(chats);
                                    });
                            }
                        }
                    });
            } catch (error) {}
            // generateGroupedChat();
        }
        return () => (mounted = false);
    }, []);

    /*  UI Events Methods   */

    const goToUserDetails = () => {
        props.navigation.navigate(Routes.UserDetails, {
            otherUserUid: otherUid,
        });
    };

    const checkUserExist = () => {
        if (user) {
            database()
                .ref(`allUsers`)
                .once('value')
                .then((snap) => {
                    if (userId === user.uid && snap.hasChild(`${otherUid}`)) {
                        database()
                            .ref(`allUsers/${userId}`)
                            .once('value')
                            .then((snap) => {
                                setSenderName(
                                    `${snap.val().firstName} ${
                                        snap.val().lastName
                                    }`,
                                );

                                return `${snap.val().firstName} ${
                                    snap.val().lastName
                                }`;
                            });
                        database()
                            .ref(`allUsers/${otherUid}`)
                            .once('value')
                            .then((snap) => {
                                setReceiverName(
                                    `${snap.val().firstName} ${
                                        snap.val().lastName
                                    }`,
                                );
                                if (snap.val().profileImageUrl) {
                                    setImageUri(snap.val().profileImageUrl);
                                }
                                return `${snap.val().firstName} ${
                                    snap.val().lastName
                                }`;
                            });
                    } else {
                        ToastAndroid.show('User NOT Found', ToastAndroid.LONG);
                        props.navigation.dispatch(
                            CommonActions.reset({
                                index: 0,
                                routes: [{name: Routes.Home}],
                            }),
                        );
                    }
                });
        } else {
            props.navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [{name: Routes.UnAuthenticated}],
                }),
            );
        }
        return true;
    };

    const onSend = (
        sendContent = content,
        contactName = '',
        downloadUrl = '',
        type = 'text',
    ) => {
        console.log(
            'sendContent ::: ',
            sendContent,
            'contactName ::: ',
            contactName,
            ' type ::: ',
            type,
            ' downloadUrl :: ',
            downloadUrl,
        );

        if (sendContent.trim() !== '') {
            database()
                .ref(`users/${userId}`)
                .once('value')
                .then((snap) => {
                    if (snap.hasChild(`${otherUid}`)) {
                        database()
                            .ref(`users/${userId}/${otherUid}`)
                            .update({
                                type: type,
                                content:
                                    type === 'image'
                                        ? 'image'
                                        : type === 'contact'
                                        ? contactName
                                        : sendContent.trim(),
                                timestamp: Date.now(),
                                uid: userId,
                                downloadUrl,
                            });
                        database()
                            .ref(`users/${otherUid}/${userId}`)
                            .update({
                                type: type,
                                content:
                                    type === 'image'
                                        ? 'image'
                                        : type === 'contact'
                                        ? contactName
                                        : sendContent.trim(),
                                timestamp: Date.now(),
                                uid: userId,
                                downloadUrl,
                                contactName,
                            });
                        let temp = snap.val();
                        let key = temp[otherUid].chatKey;
                        database().ref(`chats/${key}`).push({
                            type: type,
                            content: sendContent.trim(),
                            timestamp: Date.now(),
                            uid: userId,
                            downloadUrl,
                            contactName,
                        });
                    } else {
                        database()
                            .ref(`users/${userId}/${otherUid}`)
                            .set({
                                type: type,
                                chatKey: chatKey,
                                otherPersonName: receiverName,
                                otherPersonUid: otherUid,
                                content:
                                    type === 'image'
                                        ? 'image'
                                        : type === 'contact'
                                        ? contactName
                                        : sendContent.trim(),
                                timestamp: Date.now(),
                                uid: userId,
                                downloadUrl,
                                contactName,
                            });
                        database()
                            .ref(`users/${otherUid}/${userId}`)
                            .set({
                                type: type,
                                chatKey: chatKey,
                                otherPersonName: senderName,
                                otherPersonUid: userId,
                                content:
                                    type === 'image'
                                        ? 'image'
                                        : type === 'contact'
                                        ? contactName
                                        : sendContent.trim(),
                                timestamp: Date.now(),
                                uid: userId,
                                downloadUrl,
                                contactName,
                            });
                        database().ref(`chats/${chatKey}`).push({
                            type: type,
                            content: sendContent.trim(),
                            timestamp: Date.now(),
                            uid: userId,
                            downloadUrl,
                            contactName,
                        });
                    }
                });
            setContent('');
            setContainsText(false);
        }
    };

    // To get Time Format
    const getTimeFormat = (timeStamp) => {
        let d = new Date(timeStamp);
        let hours = d.getHours();
        let minutes = d.getMinutes();
        let meridiem = hours >= 12 ? 'PM' : 'AM';

        // Find current hour in AM-PM Format
        hours = hours % 12;

        // To display "0" as "12"
        hours = hours ? hours : 12;
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        let formattedTime = `${hours}:${minutes} ${meridiem}`;
        return formattedTime;
    };

    const handleContent = (text) => {
        setContent(text);
        if (text.trim() != '') {
            setContainsText(true);
        } else {
            setContainsText(false);
        }
    };

    const getYesterDayDate = () => {
        let yesterDayDate = new Date();
        yesterDayDate.setDate(yesterDayDate.getDate() - 1);
        return yesterDayDate;
    };

    // To Get Tag for Display date,today and yesterday
    const getTag = (date) => {
        let displayTag = '';

        if (
            moment(date).format('LL') ===
            moment(getYesterDayDate()).format('LL')
        ) {
            displayTag = Strings.yesterday;
        } else if (
            moment(date).format('LL') === moment(new Date()).format('LL')
        ) {
            displayTag = Strings.today;
        } else {
            displayTag = moment(date).format('LL');
        }
        return displayTag;
    };

    const generateGroupedChat = (chatArr) => {
        let copyChats = chatArr.map((chat, index, arr) => {
            let objChat = {...chat};
            if (index === 0) {
                objChat = {
                    type: 'dayTag',
                    tag: getTag(chat?.timestamp),
                    ...objChat,
                };
            }

            if (index > 0 && index < chatArr.length) {
                if (
                    moment(chat?.timestamp).format('LL') !==
                    moment(arr[index - 1].timestamp).format('LL')
                ) {
                    objChat = {
                        type: 'dayTag',
                        tag: getTag(chat?.timestamp),
                        ...objChat,
                    };
                }
            }
            return objChat;
        });
        setChats(copyChats);
    };

    const handleShare = async () => {
        setVisible(true);
    };

    const onCheckStatus = async (paymentResponse) => {
        setPaymentVisible(false);
        setPaymentStatus('Please wait while confirming your payment!');
        setResponse(paymentResponse);

        let jsonResponse = JSON.parse(paymentResponse);
        console.log('jsonResponse :::: ', jsonResponse);

        //http://localhost:8000/payment
        //https://api.stripe.com/v1/charges/sk_test_51Jolg5SCS1HgBlUb2ChkKUQMIT0uVHtSHkYlOgyDju7jTeBOGe9nPOHtfQT0mZDTSa65fZcNcIQcUxnN7Z0Wrdt800bepSDTmh
        try {
            const stripeResponse = await axios.post(
                'http://localhost:8000/payment',
                {
                    email: 'asmeetar@webmob.tech',
                    product: cartInfo,
                    authToken: jsonResponse,
                },
            );

            console.log('stripeResponse :: ', stripeResponse);
            if (stripeResponse) {
                const {paid} = stripeResponse.data;
                if (paid === true) {
                    setPaymentStatus('Payment Success');
                } else {
                    setPaymentStatus('Payment failed due to some issue');
                }
            } else {
                setPaymentStatus(' Payment failed due to some issue');
            }
        } catch (error) {
            console.log('PAYMENT error ::: ', error);
            setPaymentStatus(' Payment failed due to some issue');
            setPaymentStatus(null);
        }
    };

    const openCamera = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
            saveToPhotos: true,
        };
        launchCamera(options, (response) => {
            if (response.didCancel) {
                // setImageUri(imageUri);
            } else if (response.errorCode == 'camera_unavailable') {
                // setImageUri(imageUri);
            } else if (response.errorCode == 'permission') {
                // setImageUri(imageUri);
            } else if (response.errorCode == 'others') {
                // setImageUri(imageUri);
            } else {
                setContent(response?.uri);
                uploadImage(response?.uri);
            }
        });
    };

    const openGallery = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
        };
        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                setImageUri(imageUri);
            } else if (response.errorCode == 'camera_unavailable') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'permission') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'others') {
                setImageUri(imageUri);
            } else {
                // console.log('GALLERY response ::: ', response);
                setContent(response?.uri);
                uploadImage(response?.uri);
            }
        });
    };

    const uploadImage = (uri) => {
        let imageName = 'Chats_' + Date.now();

        storage()
            .ref(`${chatKey}/${imageName}`)
            .putFile(uri.toString())
            .then(() => {
                console.log('IMAGE UPLOADED TO STORAGE');
                storage()
                    .ref(`${chatKey}/${imageName}`)
                    .getDownloadURL()
                    .then((downloadUrl) => {
                        onSend(downloadUrl, '', '', 'image');
                    });
            });
    };

    const uploadDocument = async (res) => {
        RNFS.copyFile(
            res[0].uri,
            `${RNFS.TemporaryDirectoryPath}/${res[0].name}`,
        )
            .then(() => {
                console.log('COPY SUCCESS ');
                const documentUri = `file://${RNFS.TemporaryDirectoryPath}/${res[0].name}`;

                try {
                    const storageRef = storage().ref();
                    storageRef
                        .child(chatKey)
                        .child(res[0]?.name)
                        .putFile(documentUri.toString())
                        .then(() => {
                            console.log('DOCUMENT UPLOADED TO STORAGE');
                            storage()
                                .ref(`${chatKey}/${res[0].name}`)
                                .getDownloadURL()
                                .then((downloadUrl) => {
                                    onSend(
                                        res[0]?.name,
                                        '',
                                        downloadUrl,
                                        res[0].type,
                                    );
                                });
                        });
                } catch (error) {
                    console.log('error ::: ', error);
                }
            })
            .catch((er) => console.log('er :: ', er));
    };

    const uploadAudio = async (res) => {
        RNFS.copyFile(
            res[0].uri,
            `${RNFS.TemporaryDirectoryPath}/${res[0].name}`,
        )
            .then(() => {
                console.log('COPY SUCCESS ');

                const documentUri = `file://${RNFS.TemporaryDirectoryPath}/${res[0].name}`;
                const songName = res[0]?.name.replaceAll('%20', ' ');

                try {
                    const storageRef = storage().ref();
                    storageRef
                        .child(chatKey)
                        .child(res[0]?.name)
                        .putFile(documentUri.toString())
                        .then(() => {
                            console.log('AUDIO UPLOADED TO STORAGE');
                            storage()
                                .ref(`${chatKey}/${res[0].name}`)
                                .getDownloadURL()
                                .then((downloadUrl) => {
                                    onSend(
                                        songName,
                                        '',
                                        downloadUrl,
                                        res[0].type,
                                    );
                                });
                        });
                } catch (error) {
                    console.log('error ::: ', error);
                }
            })
            .catch((er) => console.log('er :: ', er));
    };

    const handleSharingOptionPress = async (type) => {
        setVisible(false);
        switch (type) {
            case 'DOCUMENT':
                // Pick a single file
                try {
                    const res = await DocumentPicker.pick({
                        type: [DocumentPicker.types.allFiles],
                    });
                    uploadDocument(res);
                } catch (err) {
                    if (DocumentPicker.isCancel(err)) {
                        // User cancelled the picker, exit any dialogs or menus and move on
                    } else {
                        throw err;
                    }
                }
                break;

            case 'CAMERA':
                PermissionUtils.requestCameraPermission().then((isGranted) => {
                    //it will return true if permission granted otherwise false
                    if (isGranted) {
                        openCamera();
                    }
                });
                break;

            case 'GALLERY':
                /* For Storage Permission */
                PermissionUtils.requestStoragePermission().then((isGranted) => {
                    //it will return true if permission granted otherwise false
                    if (isGranted) {
                        openGallery();
                    }
                });
                break;

            case 'AUDIO':
                // Pick a single file
                try {
                    const res = await DocumentPicker.pick({
                        type: [DocumentPicker.types.audio],
                    });
                    uploadAudio(res);
                } catch (err) {
                    if (DocumentPicker.isCancel(err)) {
                        // User cancelled the picker, exit any dialogs or menus and move on
                    } else {
                        throw err;
                    }
                }
                break;

            case 'PAYMENT':
                setPaymentVisible(true);
                break;

            case 'LOCATION':
                console.log('LOCATION');
                break;

            case 'CONTACT':
                selectContactPhone().then((selection) => {
                    if (!selection) {
                        return null;
                    }

                    let {contact, selectedPhone} = selection;

                    onSend(selectedPhone.number, contact.name, '', 'contact');
                    return selectedPhone.number;
                });
                break;

            default:
                break;
        }
    };

    const handleMusic = (timestamp, url) => {
        if (chatId !== timestamp) {
            setChatId(timestamp);
            setIsPlaying(null);
        }

        try {
            isPlaying === null && SoundPlayer.playUrl(url);
            isPlaying ? SoundPlayer.pause() : SoundPlayer.resume();

            setIsPlaying(!isPlaying);
        } catch (error) {
            alert("Can't play the sound file :::: ", error);
        }
    };

    const handleChatPress = async (chat) => {
        // console.log('chat?.type ::: ', chat?.type);

        if (chat?.type === 'image') {
            props.navigation.navigate(Routes.DisplayImage, {
                uri: chat?.content,
            });
        } else if (chat?.type === 'application/pdf') {
            const localFile = `${RNFS.DocumentDirectoryPath}/${chat?.content}`;
            const options = {
                fromUrl: chat?.downloadUrl,
                toFile: localFile,
            };
            RNFS.downloadFile(options)
                .promise.then(() => FileViewer.open(localFile))
                .then(() => {
                    // success
                })
                .catch((error) => {
                    // error
                });
        }
    };

    /* Custom-Compoenet Sub-render Method */

    const renderProfileImage = () => (
        <Ripple onPress={() => goToUserDetails()}>
            <View style={styles.headerStyle}>
                <Image
                    source={
                        imageUri
                            ? {uri: imageUri}
                            : require('src/assets/images/user_placeholder_image.png')
                    }
                    style={styles.userProfileStyle}
                />
                <View>
                    <Label
                        ms={10}
                        normal
                        color={Color.WHITE}
                        numberOfLines={1}
                        bolder
                        style={styles.receiverNameStyle}>
                        {receiverName && receiverName}
                    </Label>
                </View>
            </View>
        </Ripple>
    );

    const renderChatItem = (chat) => {
        return (
            <>
                {chat?.tag ? (
                    <View style={styles.dayStyle}>
                        <Label color={Color.DARK_LIGHT_BLACK} xsmall>
                            {chat?.tag}
                        </Label>
                    </View>
                ) : null}
                {chat?.type === 'audio/mpeg' ? (
                    <View style={styles.itemStyle}>
                        <View
                            key={chat?.timestamp}
                            style={[
                                chat?.uid === userId
                                    ? styles.myStyle
                                    : styles.otherStyle,
                                styles.chatInnerCont,
                            ]}>
                            {!isPlaying ? (
                                <Ripple
                                    rippleContainerBorderRadius={50}
                                    onPress={() =>
                                        handleMusic(
                                            chat?.timestamp,
                                            chat?.downloadUrl,
                                        )
                                    }
                                    style={styles.playIconStyle}>
                                    <IonIcon
                                        name="play-circle-outline"
                                        style={styles.playIcon}
                                        color={Color.WHITE}
                                        size={ThemeUtils.relativeWidth(7)}
                                    />
                                </Ripple>
                            ) : (
                                <Ripple
                                    rippleContainerBorderRadius={50}
                                    onPress={() =>
                                        handleMusic(
                                            chat?.timestamp,
                                            chat?.downloadUrl,
                                        )
                                    }
                                    style={styles.playIconStyle}>
                                    <IonIcon
                                        name="pause-circle-outline"
                                        style={styles.playIcon}
                                        color={Color.WHITE}
                                        size={ThemeUtils.relativeWidth(7)}
                                    />
                                </Ripple>
                            )}

                            <Label
                                color={Color.WHITE}
                                small
                                bolder
                                ms={5}
                                style={{
                                    maxWidth: ThemeUtils.relativeWidth(65),
                                }}>
                                {chat?.content}
                            </Label>
                        </View>
                        <Label
                            xsmall
                            style={
                                chat?.uid === userId
                                    ? styles.myTimeStyle
                                    : styles.otherTimeStyle
                            }
                            color={Color.DARK_LIGHT_BLACK}>
                            {getTimeFormat(chat?.timestamp)}
                        </Label>
                    </View>
                ) : (
                    <View style={styles.itemStyle}>
                        <Ripple
                            key={chat?.timestamp}
                            style={[
                                chat?.uid === userId
                                    ? chat?.type === 'image'
                                        ? styles.myImageStyle
                                        : styles.myStyle
                                    : chat?.type === 'image'
                                    ? styles.otherImageStyle
                                    : styles.otherStyle,
                            ]}
                            rippleContainerBorderRadius={20}
                            onPress={() => handleChatPress(chat)}>
                            {chat?.type === 'contact' && (
                                <Label color={Color.WHITE} small bolder>
                                    {chat?.contactName}
                                </Label>
                            )}
                            {chat?.type === 'image' ? (
                                <Image
                                    source={{uri: chat?.content}}
                                    style={
                                        chat?.uid === userId
                                            ? styles.myChatImageStyle
                                            : styles.otherChatImageStyle
                                    }
                                />
                            ) : (
                                <Label
                                    color={Color.WHITE}
                                    small
                                    bolder={chat?.type === 'application/pdf'}>
                                    {chat?.content}
                                </Label>
                            )}
                        </Ripple>
                        <Label
                            xsmall
                            style={
                                chat?.uid === userId
                                    ? styles.myTimeStyle
                                    : styles.otherTimeStyle
                            }
                            color={Color.DARK_LIGHT_BLACK}>
                            {getTimeFormat(chat?.timestamp)}
                        </Label>
                    </View>
                )}
            </>
        );
    };

    const StripeTest = () => {
        const {confirmPayment} = useStripe();

        const [key, setKey] = useState('');

        useEffect(async () => {
            await fetch('http://localhost:3000/create-payment-intent', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
            })
                .then((res) => res.json())
                .then((res) => {
                    console.log('intent', res);
                    setKey(res.clientSecret);
                })
                .catch((e) =>
                    console.log('USE EFFECT ERROR MSG ::: ', e.message),
                );
        }, []);

        const handleConfirmation = async () => {
            if (key) {
                const {paymentIntent, error} = await confirmPayment(key, {
                    type: 'Card',
                    billingDetails: {
                        email: 'John@gmail.com',
                    },
                });

                if (!error) {
                    console.log(
                        'Received payment ',
                        `Billed for ${paymentIntent?.amount}`,
                    );
                } else {
                    console.log('Error :: ', error.message);
                }
            }
        };

        return (
            <View>
                <CardField
                    postalCodeEnabled={false}
                    placeholder={{
                        number: '4242 4242 4242 4242',
                    }}
                    cardStyle={{
                        backgroundColor: '#FFFFFF',
                        textColor: '#000000',
                    }}
                    style={{
                        width: '100%',
                        height: 50,
                        marginVertical: 30,
                    }}
                    onCardChange={(cardDetails) => {
                        // console.log('cardDetails', cardDetails);
                    }}
                    onFocus={(focusedField) => {
                        // console.log('focusField', focusedField);
                    }}
                />
                <AppButton
                    children="Confirm payment"
                    click={() => handleConfirmation()}
                />
            </View>
        );
    };

    const renderSharingOption = ({id, type, style, iconName, title}) => (
        <View key={id} style={styles.vwSelector}>
            <Ripple
                rippleContainerBorderRadius={50}
                onPress={() => handleSharingOptionPress(type)}
                style={[styles.chooseCameraIconStyle, style]}>
                <View>
                    <IonIcon
                        name={iconName}
                        style={styles.searchIcon}
                        color={Color.WHITE}
                        size={ThemeUtils.relativeWidth(7)}
                    />
                </View>
            </Ripple>
            <Label xsmall color={Color.WHITE}>
                {title}
            </Label>
        </View>
    );

    const renderSendContainer = () => (
        <View
            style={styles.sendContainerStyle}
            onLayout={(e) =>
                setSendContainerHeight(e.nativeEvent.layout.height)
            }>
            <AutoGrowingTextInput
                style={styles.contentStyle}
                value={content}
                onChangeText={(text) => handleContent(text)}
                placeholder={Strings.sendAMessage}
            />
            <Ripple
                rippleContainerBorderRadius={50}
                onPress={() => handleShare()}
                style={styles.attachIconStyle}>
                <IonIcon
                    name="attach-outline"
                    style={styles.attachIcon}
                    color={Color.WHITE}
                    size={ThemeUtils.relativeWidth(8)}
                />
            </Ripple>
            <Label
                bolder
                style={
                    containsText
                        ? styles.sendLabelStylewithColor
                        : styles.sendLabelStylewithoutColor
                }
                onPress={() => {
                    containsText && onSend();
                }}>
                {Strings.send}
            </Label>
        </View>
    );

    const renderSharingOptionModal = () => (
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
            onRequestClose={() => setVisible(!visible)}>
            <TouchableOpacity
                activeOpacity={1}
                style={CommonStyle.full_flex_grow}
                onPress={() => setVisible(!visible)}>
                <View style={styles.vwModal}>
                    <ScrollView
                        contentContainerStyle={styles.contentContainerStyle}>
                        <View style={styles.vwChooser}>
                            {sharingOptions?.map((item) =>
                                renderSharingOption(item),
                            )}
                        </View>
                    </ScrollView>
                </View>
            </TouchableOpacity>
        </Modal>
    );

    return (
        <>
            {checkUserExist() && (
                <StripeProvider
                    publishableKey={
                        Constants.STRIPE_API_KEYS.PUBLISHABLE_API_KEY
                    }
                    // urlScheme="your-url-scheme" // required for 3D Secure and bank redirects
                    // merchantIdentifier="merchant.identifier" // required for Apple Pay
                >
                    <View style={CommonStyle.master_full_flex}>
                        <View style={styles.container}>
                            {paymentVisible ? (
                                <PaymentView />
                            ) : (
                                <>
                                    {chats?.length > 0 && (
                                        <FlatList
                                            ref={listRef}
                                            data={chats}
                                            style={[
                                                styles.flatListStyle,
                                                {
                                                    marginBottom:
                                                        sendContainerHeight + 5,
                                                },
                                            ]}
                                            renderItem={({item}) =>
                                                renderChatItem(item)
                                            }
                                            keyExtractor={(item, index) =>
                                                index.toString()
                                            }
                                            // initialScrollIndex={chats.length - 1}
                                            onContentSizeChange={() =>
                                                listRef?.current?.scrollToEnd()
                                            }
                                        />
                                    )}
                                    {renderSharingOptionModal()}
                                    {renderSendContainer()}
                                </>
                            )}
                        </View>
                    </View>
                </StripeProvider>
            )}
        </>
    );
};

export default Chat;
