/* eslint-disable  */
import React,
{
    useState,
    useEffect,
    useLayoutEffect,

} from 'react';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {
    Controller,
    useForm
} from 'react-hook-form';
import { useIsFocused } from '@react-navigation/native';
import {
    View,
    FlatList,
    ActivityIndicator,
    Modal,
    Alert,
    Image,
} from 'react-native';
import { styles } from './styles';
import {
    Label,
    MaterialTextInput,
    RoundButton,
    Ripple,
} from 'src/component';
import {
    CommonStyle,
    Constants,
    Messages,
    ThemeUtils,
    Strings,
    Color,
    showToast
} from 'src/utils';
import Routes from 'src/router/Routes';

const ChangePassword = (props) => {

    const {
        register,
        setValue,
        control,
        reset,
        watch,
        clearErrors,
        getValues,
        errors,
        handleSubmit,
    } = useForm();

    const user = auth().currentUser;
    const [currentPassword, setCurrentPassword] = useState();
    const [newPassword, setNewPassword] = useState();
    const [confirmPasword, setConfirmPasword] = useState();
    const [userPrevPassword, setUserPrevPassword] = useState();
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        let mounted = true;

        if (mounted) {
            if (user) {
                try {

                    database().ref(`allUsers/${user.uid}`).on("value", snap => {
                        setUserPrevPassword(snap.val().password);
                    })

                } catch (error) {
                    console.log("useEffect (ChangePasswordScreen) error ==>> ", error);
                }
            }
        }

        return () => mounted = false;

    }, []);

    useEffect(() => {
        
    }, [currentPassword, newPassword, confirmPasword]);

    const reAuthenticate = () => {
        const cred = auth.EmailAuthProvider.credential(user.email, userPrevPassword);
        return user.reauthenticateWithCredential(cred);
    }

    const changePasswordinDB = async (currentPswd, newPswd, confirmPswd) => {
        if (userPrevPassword === currentPswd) {
            if (newPswd === confirmPswd) {

                await reAuthenticate().then(() => {
                    user.updatePassword(newPswd).then(() => {

                        auth().signInWithEmailAndPassword(user.email, newPswd)
                            .catch(error => {
                                showToast(Strings.somethingWentWrong);
                            });
                    });

                })
                    .then(() => {
                        database().ref(`allUsers/${user.uid}`).update({
                            password: newPswd,
                        }).then(() => {
                            setVisible(false);
                            props.navigation.navigate(Routes.Profile);
                            showToast(Strings.success, Strings.pswdChangedSuccessfully, 'success');
                        });
                    })
                    .catch(reAuthenticateError => {
                        setVisible(false);
                        showToast(Strings.somethingWentWrong);
                    });
            }
            else {
                setVisible(false);
                showToast('', Strings.newPswdConfirmPswdNotMatch, 'error');
            }
        }
        else {
            setVisible(false);
            showToast('', Messages.Errors.crntPwdNotMatch, 'error');
        }
    }

    const handleChangePassword = async (data) => {
        setVisible(true);
        await setCurrentPassword(data.currentPassword);
        await setNewPassword(data.newPassword);
        await setConfirmPasword(data.confirmPassword);

        changePasswordinDB(data.currentPassword, data.newPassword, data.confirmPassword);
    }


    return (
        <View style={styles.mainContainer}>
            <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                    <MaterialTextInput
                        onFocus={() => clearErrors('currentPassword')}
                        error={errors.currentPassword?.message}
                        value={value}
                        label={Strings.currentPassword}
                        secureTextEntry
                        onBlur={onBlur}
                        onChangeText={onChange}
                    />
                )}
                name={'currentPassword'}
                defaultValue={''}
                rules={{ required: Messages.Errors.pwdBlank }}
            />
            <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                    <MaterialTextInput
                        onFocus={() => clearErrors('newPassword')}
                        error={errors.newPassword?.message}
                        value={value}
                        label={Strings.newPassword}
                        secureTextEntry
                        onBlur={onBlur}
                        onChangeText={onChange}
                    />
                )}
                name={'newPassword'}
                defaultValue={''}
                rules={{ required: Messages.Errors.pwdBlank }}
            />

            <Controller
                control={control}
                render={({ onChange, onBlur, value }) => (
                    <MaterialTextInput
                        onFocus={() => clearErrors('confirmPassword')}
                        error={errors.confirmPassword?.message}
                        value={value}
                        label={Strings.confirmPassword}
                        secureTextEntry
                        onBlur={onBlur}
                        onChangeText={onChange}
                    />
                )}
                name={'confirmPassword'}
                defaultValue={''}
                rules={{ required: Messages.Errors.pwdBlank }}
            />

            <View style={styles.btnVw}>
                <RoundButton
                    click={handleSubmit(handleChangePassword)}
                    width={ThemeUtils.relativeRealWidth(90)}
                    mt={ThemeUtils.relativeRealHeight(4)}
                    textColor={Color.WHITE}
                    visible={visible}
                    style={styles.changePasswordBtnStyle}>

                    {Strings.changePassword}
                </RoundButton>
            </View>

        </View>
    )
}

export default ChangePassword;