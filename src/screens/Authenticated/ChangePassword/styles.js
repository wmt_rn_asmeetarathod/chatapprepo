/* eslint-disable  */

import { StyleSheet } from 'react-native';
import { Color } from 'src/utils';
import { ThemeUtils } from 'src/utils';

export const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        // justifyContent: 'center',
        flexDirection: 'column',
        padding :20,
    },
    changePasswordBtnStyle: {
        alignSelf : 'center',
      },
      btnVw:{
          justifyContent : 'center',
          alignItems : 'center',
      }
});
