import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        padding: 5,
    },
    vwMyStatus: {
        backgroundColor: Color.WHITE,
        flexDirection: 'row',
        padding: 20,
        alignItems: 'center',
    },
    statusIconStyle: {
        backgroundColor: Color.PRIMARY,
        padding: 15,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
    },
    statusIcon: {},
    labelsStyle: {
        marginStart: 15,
    },
    vwRecentUpdatesLabel: {
        margin: 10,
    },
    border: {
        borderWidth: 0.3,
        borderColor: Color.TEXT_PLACEHOLDER,
    },
    statusListStyle: {
        flex: 1,
    },
    // modal style
    modalView: {
        // backgroundColor: Color.BLACK,
        // padding: 25,
        // justifyContent: 'center',
        // alignItems: 'center',
        // alignSelf: 'center',
        // width: ThemeUtils.relativeWidth(100),
        // height: ThemeUtils.relativeHeight(100),
        // shadowColor: Color.WHITE,
        // shadowOffset: {
        //     width: 0,
        //     height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 4,
        // elevation: 3,
        flex: 1,
    },
    userImageStyle: {
        height: ThemeUtils.relativeWidth(15),
        width: ThemeUtils.relativeWidth(15),
        borderRadius: ThemeUtils.relativeWidth(7.5),
    },
    imageStyle: {
        height: ThemeUtils.relativeHeight(80),
        width: ThemeUtils.relativeWidth(100),
    },
    vwCaption: {
        width: ThemeUtils.relativeWidth(100),
        backgroundColor: Color.DARK_LIGHT_BLACK,
        position: 'absolute',
        bottom: 40,
        flex: 1,
        alignItems: 'center',
        padding: 10,
    },
    slide: {
        backgroundColor: Color.BLACK,
        justifyContent: 'center',
        flex: 1,
    },
    statusHeader: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
    },
    progressBar: {
        marginBottom: 10,
    },
    newGroupIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 20,
        bottom: 20,
        backgroundColor: Color.PRIMARY_DARK,
        borderRadius: 50,
        padding: 10,
    },
    vwEmpty: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
    },
    vwMultiStatus: {
        flexDirection: 'row',
        flex: 1,
    },
    modal: {
        flex: 1,
    },
});
