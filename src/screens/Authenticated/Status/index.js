/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useRef} from 'react';
import {
    View,
    FlatList,
    Modal,
    ScrollView,
    Image,
    BackHandler,
} from 'react-native';
import {styles} from './styles';

// Custom Component
import {Label, Ripple, StatusContainer} from 'src/component';

// images
import USER_PLACEHOLDER from 'src/assets/images/user_placeholder_image.png';

// Utils
import {Color, ThemeUtils, CommonStyle} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import Carousel from 'react-native-snap-carousel';
import moment from 'moment';
import * as Progress from 'react-native-progress';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import StoryContainer from 'react-native-stories-media/src/StoryContainer';

const Status = (props) => {
    const user = auth().currentUser;
    const carouselRef = useRef();
    const [visibleIndex, setVisibleIndex] = useState(0);
    const [myStatus, setMyStatus] = useState([]);
    const [otherStatusList, setOtherStatusList] = useState([]);
    const [isMyStatusPressed, setIsMyStatusPressed] = useState(false);
    const [currentUserIndex, setCurrentUserIndex] = useState(0);
    const [currentScrollValue, setCurrentScrollValue] = useState(0);
    const modalScroll = useRef(null);
    const [isModelOpen, setModel] = useState(false);
    const [currentList, setCurrentList] = useState([]);

    /* Life-cycle Methods */

    useEffect(() => {
        getStatusList();
        BackHandler.addEventListener('hardwareBackPress', onBackPress);

        return () => {
            BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        };
    }, []);

    useEffect(() => {
        isMyStatusPressed === false && setCurrentList(otherStatusList);
    }, [otherStatusList]);

    useEffect(() => {
        isMyStatusPressed
            ? setCurrentList(myStatus)
            : setCurrentList(otherStatusList);
    }, [isMyStatusPressed]);

    // at every 10 sec , check status hours
    useEffect(() => {
        setInterval(() => {
            checkStoryHours();
        }, 10000);
    }, [myStatus]);

    /* UI-Events Methods */

    // Get status from database
    const getStatusList = () => {
        database()
            .ref('status')
            .on('value', (snap) => {
                let myStatusList = [];
                let othersList = [];
                snap.forEach((item) => {
                    if (item?.key === user?.uid) {
                        let obj = item?.val();
                        let storiesArray = [];
                        item?.forEach((data) => {
                            if (data?.key === 'stories') {
                                data.forEach((d) => {
                                    let story = d.val();
                                    storiesArray.push({
                                        key: story?.key,
                                        createdAt: story?.createdAt,
                                        caption: story?.caption,
                                        statusImage: story?.statusImage,
                                    });
                                });
                            }
                        });
                        myStatusList.push({
                            createdAt: obj?.createdAt,
                            image: obj?.image,
                            name: 'My Status',
                            uid: obj?.uid,
                            stories: storiesArray,
                            views: obj?.views,
                        });
                    } else {
                        let obj = item?.val();
                        let storiesArray = [];
                        item?.forEach((data) => {
                            if (data?.key === 'stories') {
                                data.forEach((d) => {
                                    let story = d.val();
                                    storiesArray.push({
                                        key: story?.key,
                                        createdAt: story?.createdAt,
                                        caption: story?.caption,
                                        statusImage: story?.statusImage,
                                    });
                                });
                            }
                        });
                        othersList.push({
                            createdAt: obj?.createdAt,
                            image: obj?.image,
                            name: obj?.name,
                            uid: obj?.uid,
                            stories: storiesArray,
                            views: obj?.views,
                        });
                    }
                });
                setOtherStatusList(changeStructure(othersList));
                // setMyStatus(myStatusList);
                setMyStatus(changeStructure(myStatusList));
            });
    };

    const changeStructure = (arr) => {
        let newArray = [];
        arr?.map((item) => {
            let storyArray = [];
            item?.stories?.map((story) => {
                storyArray.push({
                    id: story?.key,
                    url: story?.statusImage,
                    type: 'image',
                    duration: 2,
                    isReadMore: false,
                    created: story?.createdAt,
                });
            });
            newArray.push({
                username: item?.name,
                title: 'story',
                profile: item?.image,
                stories: storyArray,
                uid: item?.uid,
                views: item?.views,
            });
        });
        return newArray;
    };

    const checkStoryHours = () => {
        if (myStatus?.length > 0) {
            let storyArr = myStatus[0]?.stories?.filter(
                (item) => Math.abs(item?.created - Date.now()) / 36e5 >= 24,
            );
            removeStory(storyArr);
        }
    };

    const onBackPress = () => {
        setIsMyStatusPressed(false);
        props.navigation.goBack();
        return true;
    };

    const onStatusPress = (index) => {
        setModel(!isModelOpen);
        setTimeout(() => {
            carouselRef?.current?.snapToItem(index);
        }, 250);
    };

    const createStatus = () => {
        props.navigation.navigate(Routes.CaptureStatus);
    };

    // delete story from database
    const removeStory = (stories) => {
        if (stories?.length > 0) {
            stories?.map((item) => {
                database()
                    .ref(`status/${user?.uid}/stories/${item?.id}`)
                    .remove()
                    .then(() => console.log('STORY DELETED'));
            });
        }
    };

    const onStorySelect = (index) => {
        setCurrentUserIndex(index);
        setModel(true);
    };

    const onStoryClose = () => {
        isMyStatusPressed && setIsMyStatusPressed(false);
        setModel(false);
    };

    const onStoryNext = (statusItem, isScroll) => {
        const {uid} = statusItem?.item;
        database()
            .ref(`status/${uid}`)
            .once('value', (snapshot) => {
                console.log('SNAPSHOT ', snapshot.val());
                let viewArr = snapshot.val()?.views;
                console.log('[...viewArr, user?.uid] ', [
                    ...viewArr,
                    user?.uid,
                ]);
                database()
                    .ref(`status/${uid}`)
                    .update({
                        views: [...viewArr, user?.uid],
                    });
            })

            .then(() => {
                console.log('VIEWS ADDED');
                const newIndex = currentUserIndex + 1;
                if (currentList?.length - 1 > currentUserIndex) {
                    setCurrentUserIndex(newIndex);
                    try {
                        if (!isScroll) {
                            modalScroll.current.snapToItem(newIndex, true);
                        }
                    } catch (e) {}
                } else {
                    onStoryClose();
                }
            });
    };

    const onStoryPrevious = (isScroll) => {
        const newIndex = currentUserIndex - 1;
        if (currentUserIndex > 0) {
            setCurrentUserIndex(newIndex);
            if (!isScroll) {
                modalScroll.current.snapToItem(newIndex, true);
            }
        }
    };

    const onScrollChange = (scrollValue) => {
        if (currentScrollValue > scrollValue) {
            // onStoryNext(true);
            setCurrentScrollValue(scrollValue);
        }
        if (currentScrollValue < scrollValue) {
            onStoryPrevious(false);
            setCurrentScrollValue(scrollValue);
        }
    };

    const isStatusSeen = (uid, views) => {
        return views?.includes(uid);
    };

    /* Custom- Component Render Methods */
    const renderItemSeparator = () => <View style={styles.border} />;

    const RenderSingleStatus = (item) => {
        const {uid, name, image, stories, createdAt} = item?.item;
        const {story} = item;
        const [progress, setProgress] = useState(0);

        let listLength = isMyStatusPressed
            ? myStatus?.length
            : otherStatusList?.length;

        useEffect(() => {
            // setTimeout(() => {
            //     if (item?.index === listLength - 1) {
            //         isMyStatusPressed
            //             ? setIsMyStatusPressed(false)
            //             : setModel(false);
            //     }
            // }, 5000);
        }, []);

        useEffect(() => {
            if (visibleIndex == item.index || visibleIndex == 0) {
                let progress = 0;
                setProgress(0);
                setInterval(() => {
                    progress += 0.4;
                    if (progress > 1) {
                        progress = 1;
                    }
                    setProgress(progress);
                }, 500);
            }
        }, []);

        return (
            <View style={styles.slide}>
                <Progress.Bar
                    progress={progress}
                    width={ThemeUtils.relativeWidth(100) / stories?.length}
                    height={1}
                    borderRadius={0}
                    borderWidth={0}
                    color={Color.WHITE}
                    unfilledColor="gray"
                    style={styles.progressBar}
                />
                <View style={styles.statusHeader}>
                    <Image
                        source={image ? {uri: image} : USER_PLACEHOLDER}
                        style={styles.userImageStyle}
                    />
                    <View style={styles.labelsStyle}>
                        <Label color={Color.WHITE} style={styles.title} bolder>
                            {name}
                        </Label>
                        <Label color={Color.WHITE} style={styles.title} xsmall>
                            {moment(story?.createdAt)
                                .calendar()
                                .replace('at', ',')}
                        </Label>
                    </View>
                </View>

                <Image
                    source={{uri: story?.statusImage}}
                    style={styles.imageStyle}
                    resizeMode="contain"
                />

                {story?.caption && (
                    <View style={styles.vwCaption}>
                        <Label color={Color.WHITE}>{story?.caption}</Label>
                    </View>
                )}
            </View>
        );
    };

    const RenderMultipleStatus = ({item}) => {
        const {uid, name, image, stories, createdAt} = item?.item;
        const [progress, setProgress] = useState(0);

        let listLength = isMyStatusPressed
            ? myStatus?.length
            : otherStatusList?.length;

        useEffect(() => {
            // setTimeout(() => {
            //     if (item?.index === listLength - 1) {
            //         isMyStatusPressed
            //             ? setIsMyStatusPressed(false)
            //             : setModel(false);
            //     }
            // }, 5000);
        }, []);

        useEffect(() => {
            if (visibleIndex == item.index || visibleIndex == 0) {
                let progress = 0;
                setProgress(0);
                setInterval(() => {
                    progress += 0.4;
                    if (progress > 1) {
                        progress = 1;
                    }
                    setProgress(progress);
                }, 500);
            }
        }, []);

        return (
            <View style={styles.vwMultiStatus}>
                {stories?.map((story) => (
                    <RenderSingleStatus
                        item={item?.item}
                        story={story}
                        index={item?.index}
                        key={story?.key}
                    />
                ))}
            </View>
        );
    };

    const renderEmptyCompo = () => (
        <View style={styles.vwEmpty}>
            <Label small>{'No Recent Updates'}</Label>
        </View>
    );

    const renderStatusModal = () => (
        <Modal
            animationType={'slide'}
            transparent={false}
            visible={isModelOpen}
            style={styles.modal}
            onShow={() => {
                if (currentUserIndex > 0) {
                    modalScroll.current.snapToItem(currentUserIndex, false);
                }
            }}
            onRequestClose={onStoryClose}>
            <Carousel
                ref={modalScroll}
                data={currentList}
                renderItem={(item, index) => (
                    <StoryContainer
                        key={item?.item?.title}
                        onClose={onStoryClose}
                        onStoryNext={() => onStoryNext(item)}
                        onStoryPrevious={onStoryPrevious}
                        dataStories={item?.item}
                        isNewStory={index !== currentUserIndex}
                        textReadMore={false}
                    />
                )}
                sliderWidth={ThemeUtils.relativeWidth(100)}
                itemWidth={ThemeUtils.relativeWidth(100)}
                autoplay
                autoplayDelay={1000}
                autoplayInterval={3000}
                onSnapToItem={(index) =>
                    index > 0 ? onScrollChange(index) : null
                }
            />
        </Modal>
    );

    return (
        <>
            <View style={CommonStyle.full_flex_grow}>
                {isModelOpen ? (
                    renderStatusModal()
                ) : (
                    <>
                        {myStatus?.length > 0 ? (
                            <StatusContainer
                                item={myStatus[0]}
                                index={0}
                                onStatusPress={(i) => {
                                    setIsMyStatusPressed(true);
                                    onStorySelect(i);
                                }}
                                seen={isStatusSeen(
                                    myStatus[0]?.uid,
                                    myStatus[0]?.views,
                                )}
                            />
                        ) : (
                            <View style={styles.vwMyStatus}>
                                <Ripple
                                    rippleContainerBorderRadius={50}
                                    onPress={() => createStatus()}
                                    style={styles.statusIconStyle}>
                                    <IonIcon
                                        name="add-outline"
                                        style={styles.statusIcon}
                                        color={Color.WHITE}
                                        size={30}
                                    />
                                </Ripple>
                                <View style={styles.labelsStyle}>
                                    <Label color={Color.BLACK} bolder>
                                        {'My Status'}
                                    </Label>
                                    <Label color="gray" small>
                                        {'tap to add/update status'}
                                    </Label>
                                </View>
                            </View>
                        )}
                        <View style={styles.vwRecentUpdatesLabel}>
                            <Label color={Color.PRIMARY} small bolder>
                                {'Recent Updates'}
                            </Label>
                        </View>

                        <FlatList
                            style={styles.statusListStyle}
                            data={otherStatusList}
                            nestedScrollEnabled={false}
                            renderItemSeparatorComponent={() =>
                                renderItemSeparator()
                            }
                            renderItem={({item, index}) => (
                                <StatusContainer
                                    item={item}
                                    index={index}
                                    onStatusPress={(i) => onStorySelect(i)}
                                    seen={isStatusSeen(user?.uid, item?.views)}
                                />
                            )}
                            keyExtractor={(item) => item?.createdAt?.toString()}
                            ListEmptyComponent={() => renderEmptyCompo()}
                        />
                    </>
                )}
            </View>
            <Ripple
                rippleContainerBorderRadius={50}
                onPress={() => createStatus()}
                style={styles.newGroupIconStyle}>
                <IonIcon
                    name="add-outline"
                    style={styles.vwGroupChat}
                    color={Color.WHITE}
                    size={30}
                />
            </Ripple>
        </>
    );
};

export default Status;
