/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';

import {View, Image, TextInput, Modal} from 'react-native';

import {styles} from './styles';

// Custom Component
import {Label, Ripple} from 'src/component';

// utils
import {Strings, Color, ThemeUtils, PermissionUtils} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import {Controller, useForm} from 'react-hook-form';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

const CreateGroup = (props) => {
    const {
        control,
        clearErrors,
        formState: {errors},
        handleSubmit,
    } = useForm();
    const users = props?.route?.params?.selectedUsers;
    const user = auth().currentUser;
    const [loggedUserName, setLoggedUserName] = useState(null);
    const [loggedUserImageUrl, setLoggedUserImageUrl] = useState(null);
    const [visible, setVisible] = useState(false);
    const [imageUri, setImageUri] = useState(null);
    const [newRef, setNewRef] = useState(null);

    /*  Life-cycles Methods */
    useEffect(() => {
        newRef === null &&
            setNewRef(database().ref(`groups/${user?.uid}`).push());
        database()
            .ref(`allUsers/${user?.uid}`)
            .once('value')
            .then((snapshot) => {
                setLoggedUserName(
                    snapshot.val().firstName + ' ' + snapshot.val().lastName,
                );
                setLoggedUserImageUrl(snapshot.val().profileImageUrl);
            });

        return () => {};
    }, []);

    /* UI-Events Methods */

    // return participants other than passed id and add logged user data
    const getParticipants = (id) => {
        let filtered = users?.filter((item) => item.uid !== id);
        filtered = [
            ...filtered,
            {
                imageUrl: loggedUserImageUrl,
                uid: user?.uid,
                name: loggedUserName,
            },
        ];
        return filtered;
    };

    const createGroup = (data) => {
        if (data?.groupName?.trim() !== '') {
            newRef.set({
                chatKey: newRef?.key,
                createdAt: Date.now(),
                timestamp: Date.now(),
                groupName: data?.groupName?.trim(),
                groupImageUrl: imageUri,
                uid: user?.uid,
                participants: users,
            });
            users?.map((item) => {
                database()
                    .ref(`groups/${item?.uid}/${newRef.key}`)
                    .set({
                        chatKey: newRef?.key,
                        createdAt: Date.now(),
                        timestamp: Date.now(),
                        groupName: data?.groupName?.trim(),
                        groupImageUrl: imageUri,
                        uid: item?.uid,
                        participants: getParticipants(item?.uid),
                    });
            });
            props.navigation.navigate(Routes.GroupChat, {chatKey: newRef?.key});
        }
    };

    const handleCamera = () => {
        setVisible(true);
    };

    const toggleBottomNavigationView = () => {
        setVisible(!visible);
    };

    const storeImageToDB = (imageUrl) => {
        const userRef = storage().ref(newRef?.key);

        userRef.putFile(imageUrl.toString()).on('state_changed', (snapshot) => {
            switch (snapshot.state) {
                case 'success':
                    userRef.getDownloadURL().then((url) => {
                        setImageUri(url);
                    });
                    break;

                default:
                    break;
            }
        });
    };

    const handleCapturePhoto = () => {
        setVisible(false);

        PermissionUtils.requestCameraPermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openCamera();
            }
        });
    };

    const handleChoosePhoto = () => {
        setVisible(false);

        /* For Storage Permission */
        PermissionUtils.requestStoragePermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openGallery();
            }
        });
    };

    const openCamera = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
            saveToPhotos: true,
        };
        launchCamera(options, (response) => {
            if (response.didCancel) {
                setImageUri(imageUri);
            } else if (response.errorCode == 'camera_unavailable') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'permission') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'others') {
                setImageUri(imageUri);
            } else {
                storeImageToDB(response.uri);
            }
        });
    };

    const openGallery = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
        };
        launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                setImageUri(imageUri);
            } else if (response.errorCode == 'camera_unavailable') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'permission') {
                setImageUri(imageUri);
            } else if (response.errorCode == 'others') {
                setImageUri(imageUri);
            } else {
                // setImageUri(imageUri);
                storeImageToDB(response.uri);
            }
        });
    };

    return (
        <View style={styles.container}>
            <View style={styles.vwTopContainer}>
                <View style={styles.vwTopFirstContainer}>
                    {imageUri === null ? (
                        <Ripple
                            rippleContainerBorderRadius={50}
                            onPress={() => handleCamera()}
                            style={styles.cameraIconStyle}>
                            <IonIcon
                                name="camera"
                                color={Color.WHITE}
                                size={ThemeUtils.relativeHeight(5)}
                            />
                        </Ripple>
                    ) : (
                        <Ripple
                            rippleContainerBorderRadius={ThemeUtils.relativeWidth(
                                7.5,
                            )}
                            onPress={() => handleCamera()}
                            style={styles.userProfileStyle}>
                            <Image
                                source={{uri: imageUri}}
                                style={styles.groupProfileStyle}
                                height={ThemeUtils.relativeWidth(15)}
                                width={ThemeUtils.relativeWidth(15)}
                            />
                        </Ripple>
                    )}
                    <Controller
                        control={control}
                        render={({onChange, onBlur, value}) => (
                            <View>
                                <TextInput
                                    onFocus={() => clearErrors('groupName')}
                                    value={value}
                                    placeholder={Strings.enterGroupName}
                                    onChangeText={(e) => onChange(e)}
                                    returnKeyType="done"
                                    onSubmitEditing={handleSubmit(createGroup)}
                                    placeholderTextColor={
                                        Color.TEXT_PLACEHOLDER
                                    }
                                    selectionColor={Color.PRIMARY_DARK}
                                    style={styles.groupNameInputStyle}
                                />
                                {errors.groupName?.message !== '' && (
                                    <Label xsmall color={Color.ERROR_COLOR}>
                                        {errors.groupName?.message}
                                    </Label>
                                )}
                            </View>
                        )}
                        name={'groupName'}
                        defaultValue={''}
                        rules={{
                            required: 'Group name is required',
                        }}
                    />
                </View>
                <Label color={Color.DARK_LIGHT_BLACK} small>
                    {Strings.groupDetailsMessage}
                </Label>
            </View>
            <View style={styles.vwBottomContainer}>
                <View style={{flexDirection: 'row'}}>
                    <Label color={Color.DARK_LIGHT_BLACK}>
                        {Strings.participants + ' : '}
                    </Label>
                    <Label color={Color.DARK_LIGHT_BLACK}>
                        {users?.length}
                    </Label>
                </View>
                <View style={styles.vwSelectedUsers}>
                    {users?.length > 0 &&
                        users?.map((user) => (
                            <View style={styles.vwUser} key={user?.uid}>
                                <Image
                                    source={
                                        user?.imageUrl
                                            ? {
                                                  uri: user?.imageUrl,
                                              }
                                            : require('src/assets/images/user_placeholder_image.png')
                                    }
                                    style={styles.userProfileStyle}
                                />
                                <Label
                                    style={{
                                        width: ThemeUtils.relativeWidth(15),
                                    }}
                                    xsmall
                                    ellipsizeMode={'tail'}
                                    numberOfLines={1}>
                                    {user?.name}
                                </Label>
                            </View>
                        ))}
                </View>
            </View>
            <Ripple
                rippleContainerBorderRadius={50}
                onPress={handleSubmit(createGroup)}
                style={styles.newGroupIconStyle}>
                <IonIcon
                    name="checkmark"
                    style={styles.vwGroupChat}
                    color={Color.WHITE}
                    size={30}
                />
            </Ripple>
            <Modal
                animationType="slide"
                transparent={true}
                visible={visible}
                onRequestClose={() => toggleBottomNavigationView()}>
                <View style={styles.vwModal}>
                    <Label bolder color={Color.WHITE} mb={5}>
                        {Strings.groupIcon}
                    </Label>
                    <View style={styles.vwChooser}>
                        <View style={styles.vwSelector}>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleCapturePhoto()}
                                style={styles.chooseCameraIconStyle}>
                                <View>
                                    <IonIcon
                                        name="camera"
                                        style={styles.searchIcon}
                                        color={Color.PRIMARY_DARK}
                                        size={ThemeUtils.relativeWidth(10)}
                                    />
                                </View>
                            </Ripple>
                            <Label xsmall color={Color.WHITE}>
                                {Strings.camera}
                            </Label>
                        </View>
                        <View style={styles.vwSelector}>
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleChoosePhoto()}
                                style={styles.chooseCameraIconStyle}>
                                <IonIcon
                                    name="image"
                                    style={styles.searchIcon}
                                    color={Color.PRIMARY_DARK}
                                    size={ThemeUtils.relativeWidth(10)}
                                />
                            </Ripple>
                            <Label xsmall color={Color.WHITE}>
                                {Strings.gallery}
                            </Label>
                        </View>
                    </View>
                </View>
            </Modal>
            {/* </View> */}

            {/* </View> */}
        </View>
    );
};

export default CreateGroup;
