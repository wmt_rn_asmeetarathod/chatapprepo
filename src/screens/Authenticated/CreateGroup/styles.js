/* eslint-disable  */

import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        flexDirection: 'column',
    },
    vwTopContainer: {
        backgroundColor: Color.WHITE,
        padding: 15,
    },
    vwTopFirstContainer: {
        flexDirection: 'row',
        marginBottom: 10,
    },
    cameraIconStyle: {
        backgroundColor: Color.TEXT_PLACEHOLDER,
        borderRadius: ThemeUtils.relativeWidth(7.5),
        marginEnd: 10,
        height: ThemeUtils.relativeWidth(15),
        width: ThemeUtils.relativeWidth(15),
        justifyContent: 'center',
        alignItems: 'center',
    },
    vwBottomContainer: {
        padding: 15,
    },
    vwUser: {
        padding: 2,

        // backgroundColor: 'purple',
    },
    groupProfileStyle: {
        width: ThemeUtils.relativeWidth(15),
        height: ThemeUtils.relativeWidth(15),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(7.5),
    },
    userProfileStyle: {
        width: ThemeUtils.relativeWidth(15),
        height: ThemeUtils.relativeWidth(15),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(7.5),
        marginEnd: 20,
    },
    vwSelectedUsers: {
        // backgroundColor: 'green',
        // height: 100,
        flexDirection: 'row',
        paddingVertical: 5,
    },
    newGroupIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 20,
        bottom: 20,
        backgroundColor: Color.PRIMARY_DARK,
        borderRadius: 50,
        padding: 10,
    },
    vwModalContainer: {
        backgroundColor: 'green',
        display: 'flex',
        justifyContent: 'flex-end',
        alignContent: 'center',
        alignSelf: 'center',
        alignItems: 'flex-end',
    },
    vwModal: {
        position: 'absolute',
        bottom: 0,
        backgroundColor: Color.PRIMARY_DARK,
        alignItems: 'flex-start',
        justifyContent: 'center',
        height: ThemeUtils.relativeHeight(18),
        width: ThemeUtils.relativeWidth(100),
        padding: 10,
    },
    vwChooser: {
        flexDirection: 'row',
    },
    chooseCameraIconStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Color.WHITE,
        borderRadius: ThemeUtils.relativeHeight(5),
        padding: 12,
    },
    vwSelector: {
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
    },
    groupNameInputStyle: {
        color: Color.PRIMARY_DARK,
        fontSize: ThemeUtils.fontLarge,
    },
});
