/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useRef} from 'react';
import {CommonActions} from '@react-navigation/native';
import {
    View,
    FlatList,
    Modal,
    Image,
    TouchableOpacity,
    ScrollView,
    TextInput,
} from 'react-native';
import {styles} from './styles';

// Custom Component
import {Label} from 'src/component';

// images
import LatterT from 'src/assets/images/T_Letter.png';

// Utils
import {Color, ThemeUtils} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import {RNCamera} from 'react-native-camera';
import CameraRoll from '@react-native-community/cameraroll';
import {
    Grayscale,
    Sepia,
    Tint,
    ColorMatrix,
    concatColorMatrices,
    invert,
    contrast,
    saturate,
} from 'react-native-color-matrix-image-filters';
import GestureRecognizer from 'react-native-swipe-gestures';

const CaptureStatus = (props) => {
    const cameraRef = useRef();
    const user = auth().currentUser;
    const [cameraType, setCameraType] = useState('back');
    const [flash, setFlash] = useState(false);
    const [deviceImages, setDeviceImages] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [statusImage, setStatusImage] = useState(null);
    const [caption, setCaption] = useState(null);
    const [filterModalVisible, setFilterModalVisible] = useState(false);
    const filterNames = ['None', 'Grayscale', 'Sepia', 'Tint', 'ColorMatrix'];
    const [selectedFilter, setSelectedFilter] = useState('None');
    const [loggedUserName, setLoggedUserName] = useState(null);
    const [loggedUserImageUrl, setLoggedUserImageUrl] = useState(null);
    const [containUid, setContainUid] = useState(false);

    /* Life-cycle Methods */

    useEffect(() => {
        database()
            .ref(`allUsers/${user?.uid}`)
            .once('value')
            .then((snapshot) => {
                setLoggedUserName(
                    snapshot.val().firstName + ' ' + snapshot.val().lastName,
                );
                setLoggedUserImageUrl(snapshot.val().profileImageUrl);
            });

        database()
            .ref('status')
            .once('value')
            .then((snapshot) => {
                if (snapshot.hasChild(user?.uid)) {
                    console.log('CONTAIN');
                    setContainUid(true);
                } else {
                    console.log('NOT  CONTAIN');
                    setContainUid(false);
                }
            });

        return () => {};
    }, []);

    useEffect(() => {
        CameraRoll.getPhotos({
            first: 20,
            assetType: 'All',
        })
            .then((res) => {
                // this.setState({data: res.edges});
                console.log('GET PHOTOS RESPONSE ::: ', res);
                setDeviceImages(res?.edges);
            })
            .catch((error) => {
                console.log(error);
            });
    }, []);

    /* UI-Events Methods */

    const takePicture = async () => {
        console.log('takePicture CALLED');
        if (cameraRef?.current) {
            const options = {quality: 0.5, base64: true};
            const data = await cameraRef?.current?.takePictureAsync(options);
            console.log('CAMERA DATA ::: ', data);
            if (data) {
                setStatusImage(data?.uri);
                setModalVisible(true);
            }
        }
    };

    const handleClosePress = () => {
        setModalVisible(false);
        setStatusImage(null);
        setSelectedFilter('None');
    };

    const handleSendPress = () => {
        console.log('SEND PRESSED!!! ', caption);
        let storyName = user?.uid + '_' + Date.now();
        const newReference = database()
            .ref(`status/${user?.uid}/stories`)
            .push();

        storage()
            .ref(`status/${user?.uid}/${storyName}`)
            .putFile(statusImage?.toString())
            .then((val) => {
                console.log('IMAGE UPLOADED TO STORAGE :::: ', val);
                storage()
                    .ref(`status/${user?.uid}/${storyName}`)
                    .getDownloadURL()
                    .then((url) => {
                        console.log('DOWNLOAD URL ::: ', url);
                        if (containUid) {
                            newReference.set({
                                key: newReference?.key,
                                createdAt: Date.now(),
                                statusImage: url,
                                caption: caption ? caption : null,
                            });
                        } else {
                            database()
                                .ref(`status/${user?.uid}`)
                                .set({
                                    uid: user?.uid,
                                    createdAt: Date.now(),
                                    name: loggedUserName,
                                    image: loggedUserImageUrl,
                                })
                                .then(() =>
                                    newReference.set({
                                        key: newReference?.key,
                                        createdAt: Date.now(),
                                        statusImage: url,
                                        caption: caption ? caption : null,
                                    }),
                                );
                        }
                        props.navigation.dispatch(
                            CommonActions.navigate({
                                name: Routes.Status,
                            }),
                        );
                        console.log('STATUS ADDED');
                    });
            });
    };

    const onSwipeUp = (state) => {
        console.log('SWIPE UP');
        setFilterModalVisible(true);
    };

    const onSwipeDown = (state) => {
        console.log('SWIPE DOWN');
        setFilterModalVisible(false);
    };

    const selectFilter = (filter) => {
        setSelectedFilter(filter);
    };

    /* custom component render methods */

    const GrayscaledImage = ({item}) => (
        <TouchableOpacity onPress={() => selectFilter(item)}>
            <View style={styles.vwRenderFilter}>
                <Grayscale>
                    <View>
                        <Image
                            source={{
                                uri: statusImage && statusImage,
                            }}
                            height={ThemeUtils.relativeWidth(50)}
                            width={ThemeUtils.relativeWidth(25)}
                            style={
                                selectedFilter === item
                                    ? styles.selectedFilterImageStyle
                                    : styles.filterImageStyle
                            }
                        />
                        {selectedFilter === item && (
                            <IonIcon
                                name="checkmark-outline"
                                style={styles.checkMarkIcon}
                                color={Color.INPUT_BACKGROUND}
                                size={ThemeUtils.relativeWidth(5)}
                            />
                        )}
                    </View>
                </Grayscale>
                <Label xsmall color={Color.WHITE}>
                    {item}
                </Label>
            </View>
        </TouchableOpacity>
    );

    const TintImage = ({item}) => (
        <TouchableOpacity onPress={() => selectFilter(item)}>
            <View style={styles.vwRenderFilter}>
                <Tint amount={1.25}>
                    <View>
                        <Image
                            source={{
                                uri: statusImage && statusImage,
                            }}
                            height={ThemeUtils.relativeWidth(50)}
                            width={ThemeUtils.relativeWidth(25)}
                            style={
                                selectedFilter === item
                                    ? styles.selectedFilterImageStyle
                                    : styles.filterImageStyle
                            }
                        />
                        {selectedFilter === item && (
                            <IonIcon
                                name="checkmark-outline"
                                style={styles.checkMarkIcon}
                                color={Color.INPUT_BACKGROUND}
                                size={ThemeUtils.relativeWidth(5)}
                            />
                        )}
                    </View>
                </Tint>
                <Label xsmall color={Color.WHITE}>
                    {item}
                </Label>
            </View>
        </TouchableOpacity>
    );

    const SepiaImage = ({item}) => (
        <TouchableOpacity onPress={() => selectFilter(item)}>
            <View style={styles.vwRenderFilter}>
                <Sepia>
                    <View>
                        <Image
                            source={{
                                uri: statusImage && statusImage,
                            }}
                            height={ThemeUtils.relativeWidth(50)}
                            width={ThemeUtils.relativeWidth(25)}
                            style={
                                selectedFilter === item
                                    ? styles.selectedFilterImageStyle
                                    : styles.filterImageStyle
                            }
                        />
                        {selectedFilter === item && (
                            <IonIcon
                                name="checkmark-outline"
                                style={styles.checkMarkIcon}
                                color={Color.INPUT_BACKGROUND}
                                size={ThemeUtils.relativeWidth(5)}
                            />
                        )}
                    </View>
                </Sepia>
                <Label xsmall color={Color.WHITE}>
                    {item}
                </Label>
            </View>
        </TouchableOpacity>
    );

    const ColorMatrixImage = ({item}) => (
        <TouchableOpacity onPress={() => selectFilter(item)}>
            <View style={styles.vwRenderFilter}>
                <ColorMatrix
                    matrix={concatColorMatrices([
                        saturate(-0.9),
                        contrast(5.2),
                        invert(),
                    ])}
                    // alt: matrix={[saturate(-0.9), contrast(5.2), invert()]}
                >
                    <View>
                        <Image
                            source={{
                                uri: statusImage && statusImage,
                            }}
                            height={ThemeUtils.relativeWidth(50)}
                            width={ThemeUtils.relativeWidth(25)}
                            style={
                                selectedFilter === item
                                    ? styles.selectedFilterImageStyle
                                    : styles.filterImageStyle
                            }
                        />
                        {selectedFilter === item && (
                            <IonIcon
                                name="checkmark-outline"
                                style={styles.checkMarkIcon}
                                color={Color.INPUT_BACKGROUND}
                                size={ThemeUtils.relativeWidth(5)}
                            />
                        )}
                    </View>
                </ColorMatrix>
                <Label xsmall color={Color.WHITE}>
                    {item}
                </Label>
            </View>
        </TouchableOpacity>
    );

    const renderFilters = (item) => {
        switch (item) {
            case 'Grayscale':
                return <GrayscaledImage item={item} />;

            case 'Grayscale':
                return <SepiaImage item={item} />;

            case 'Tint':
                return <TintImage item={item} />;

            case 'ColorMatrix':
                return <ColorMatrixImage item={item} />;

            default:
                return (
                    <TouchableOpacity onPress={() => selectFilter(item)}>
                        <View style={styles.vwRenderFilter}>
                            <View>
                                <Image
                                    source={{
                                        uri: statusImage && statusImage,
                                    }}
                                    height={ThemeUtils.relativeWidth(50)}
                                    width={ThemeUtils.relativeWidth(25)}
                                    style={
                                        selectedFilter === item
                                            ? styles.selectedFilterImageStyle
                                            : styles.filterImageStyle
                                    }
                                />
                                {selectedFilter === item && (
                                    <IonIcon
                                        name="checkmark-outline"
                                        style={styles.checkMarkIcon}
                                        color={Color.INPUT_BACKGROUND}
                                        size={ThemeUtils.relativeWidth(5)}
                                    />
                                )}
                            </View>
                            <Label xsmall color={Color.WHITE}>
                                {item}
                            </Label>
                        </View>
                    </TouchableOpacity>
                );
        }
    };

    const ImageCompo = () => (
        <Image
            source={{uri: statusImage && statusImage}}
            style={styles.statusImageStyle}
            resizeMode="contain"
        />
    );

    const renderCreateStatusModal = () => (
        <GestureRecognizer
            onSwipeUp={(state) => onSwipeUp(state)}
            onSwipeDown={(state) => onSwipeDown(state)}
            config={{
                velocityThreshold: 0.3,
                directionalOffsetThreshold: 80,
            }}>
            <Modal
                visible={modalVisible}
                transparent={false}
                onRequestClose={() => setModalVisible(!modalVisible)}>
                <View style={styles.modalView}>
                    {selectedFilter === 'Grayscale' ? (
                        <Grayscale style={styles.statusImageStyle}>
                            <ImageCompo />
                        </Grayscale>
                    ) : selectedFilter === 'Sepia' ? (
                        <Sepia style={styles.statusImageStyle}>
                            <ImageCompo />
                        </Sepia>
                    ) : selectedFilter === 'Tint' ? (
                        <Tint amount={1.25} style={styles.statusImageStyle}>
                            <ImageCompo />
                        </Tint>
                    ) : selectedFilter === 'ColorMatrix' ? (
                        <ColorMatrix
                            matrix={concatColorMatrices([
                                saturate(-0.9),
                                contrast(5.2),
                                invert(),
                            ])}
                            style={styles.statusImageStyle}
                            // alt: matrix={[saturate(-0.9), contrast(5.2), invert()]}
                        >
                            <ImageCompo />
                        </ColorMatrix>
                    ) : (
                        <ImageCompo />
                    )}
                    <View style={styles.vwHeaderIcons}>
                        <TouchableOpacity
                            style={styles.closeIconStyle}
                            onPress={() => handleClosePress()}>
                            <IonIcon
                                name="close-outline"
                                style={styles.closeIcon}
                                color={Color.WHITE}
                                size={ThemeUtils.relativeWidth(10)}
                            />
                        </TouchableOpacity>
                        <View style={styles.vwHeaderRightIcons}>
                            <TouchableOpacity
                                style={styles.closeIconStyle}
                                // onPress={() => handleClosePress()}
                            >
                                <IonIcon
                                    name="crop"
                                    style={styles.closeIcon}
                                    color={Color.WHITE}
                                    size={ThemeUtils.relativeWidth(10)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.closeIconStyle}
                                // onPress={() => handleClosePress()}
                            >
                                <IonIcon
                                    name="happy-outline"
                                    style={styles.closeIcon}
                                    color={Color.WHITE}
                                    size={ThemeUtils.relativeWidth(10)}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.closeIconStyle}
                                // onPress={() => handleClosePress()}
                            >
                                <Image
                                    source={LatterT}
                                    height={ThemeUtils.relativeWidth(8)}
                                    width={ThemeUtils.relativeWidth(8)}
                                    style={styles.tLatterStyle}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.closeIconStyle}
                                // onPress={() => handleClosePress()}
                            >
                                <IonIcon
                                    name="pencil-sharp"
                                    style={styles.pencilIcon}
                                    color={Color.WHITE}
                                    size={ThemeUtils.relativeWidth(9)}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    {filterModalVisible ? (
                        <View>
                            <FlatList
                                // ref={listRef}
                                data={filterNames}
                                horizontal
                                contentContainerStyle={
                                    styles.filterFlatListStyle
                                }
                                renderItem={({item}) => renderFilters(item)}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    ) : (
                        <View>
                            <View style={styles.vwFilterLabel}>
                                <IonIcon
                                    name="chevron-up-outline"
                                    style={styles.filterIcon}
                                    color={Color.WHITE}
                                    size={ThemeUtils.relativeWidth(7)}
                                />
                                <Label color={Color.WHITE}>{'Filters'}</Label>
                            </View>

                            <View style={styles.vwTextContainer}>
                                <TextInput
                                    placeholder={'Add a Caption...'}
                                    placeholderTextColor={Color.WHITE}
                                    style={styles.inputStyle}
                                    onChangeText={(val) => setCaption(val)}
                                />
                                <TouchableOpacity
                                    style={styles.sendIconStyle}
                                    onPress={() => handleSendPress()}>
                                    <IonIcon
                                        name="send-sharp"
                                        style={styles.sendIcon}
                                        color={Color.WHITE}
                                        size={ThemeUtils.relativeWidth(7)}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    )}
                </View>
                {/* </ImageBackground> */}
            </Modal>
        </GestureRecognizer>
    );

    return (
        <>
            {renderCreateStatusModal()}
            <RNCamera
                ref={cameraRef}
                style={styles.preview}
                type={cameraType}
                captureAudio={false}
                mirrorImage={false}
                flashMode={flash ? 'on' : 'off'}>
                <View style={styles.vwOuterCapture}>
                    <View style={styles.vwDeviceImages}>
                        <View style={styles.deviceImagesStyle}>
                            <ScrollView
                                horizontal
                                showsHorizontalScrollIndicator={false}>
                                {deviceImages &&
                                    deviceImages?.map((item) => {
                                        return (
                                            <TouchableOpacity
                                                key={item?.node?.timestamp}
                                                onPress={() => {
                                                    setModalVisible(true);
                                                    setStatusImage(
                                                        item?.node?.image?.uri,
                                                    );
                                                }}>
                                                <View>
                                                    <Image
                                                        key={
                                                            item?.node
                                                                ?.timestamp
                                                        }
                                                        source={{
                                                            uri: item?.node
                                                                ?.image?.uri,
                                                        }}
                                                        height={ThemeUtils.relativeWidth(
                                                            25,
                                                        )}
                                                        width={ThemeUtils.relativeWidth(
                                                            25,
                                                        )}
                                                        style={
                                                            styles.imagesStyle
                                                        }
                                                    />
                                                    {item?.node?.type ===
                                                        'video/mp4' && (
                                                        <IonIcon
                                                            name="videocam-sharp"
                                                            style={
                                                                styles.videoIcon
                                                            }
                                                            color={Color.WHITE}
                                                            size={ThemeUtils.relativeWidth(
                                                                5,
                                                            )}
                                                        />
                                                    )}
                                                </View>
                                            </TouchableOpacity>
                                        );
                                    })}
                            </ScrollView>
                        </View>
                    </View>
                    <View style={styles.vwInnerCapture}>
                        <TouchableOpacity
                            onPress={() => setFlash(!flash)}
                            style={styles.statusIconStyle}>
                            <IonIcon
                                name={flash ? 'flash-off' : 'flash'}
                                style={styles.statusIcon}
                                color={Color.WHITE}
                                size={ThemeUtils.relativeWidth(9)}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => takePicture()}>
                            <View style={styles.capturePhoto} />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() =>
                                setCameraType(
                                    cameraType === 'back' ? 'front' : 'back',
                                )
                            }
                            style={styles.statusIconStyle}>
                            <IonIcon
                                name="sync"
                                style={styles.statusIcon}
                                color={Color.WHITE}
                                size={ThemeUtils.relativeWidth(9)}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </RNCamera>
        </>
    );
};

export default CaptureStatus;
