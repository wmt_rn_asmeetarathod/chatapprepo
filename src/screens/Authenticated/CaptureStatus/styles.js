import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    preview: {
        height: ThemeUtils.relativeHeight(100),
        width: ThemeUtils.relativeWidth(100),
        flex: 1,
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    vwOuterCapture: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        // flexDirection: 'row',

        // backgroundColor: 'green',
    },
    vwInnerCapture: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

        flexDirection: 'row',
    },
    capturePhoto: {
        borderColor: Color.WHITE,
        borderWidth: 3,
        borderRadius: ThemeUtils.relativeWidth(12.5),
        height: ThemeUtils.relativeWidth(25),
        width: ThemeUtils.relativeWidth(25),
        marginVertical: 20,
        marginHorizontal: 60,
    },
    vwDeviceImages: {
        // backgroundColor: 'purple',
    },
    deviceImagesStyle: {
        flexDirection: 'row',
        // backgroundColor: 'pink',
    },
    imagesStyle: {
        height: ThemeUtils.relativeWidth(25),
        width: ThemeUtils.relativeWidth(25),
        margin: 4,
    },
    tLatterStyle: {
        height: ThemeUtils.relativeWidth(8),
        width: ThemeUtils.relativeWidth(8),
        marginTop: 5,
        // backgroundColor: Color.WHITE,
    },
    pencilIcon: {
        marginTop: 3,
    },
    videoIcon: {
        position: 'absolute',
        bottom: 5,
        right: 10,
    },
    statusImageStyle: {
        height: ThemeUtils.relativeHeight(100),
        width: ThemeUtils.relativeWidth(100),
        flex: 1,
        backgroundColor: Color.BLACK,

        position: 'absolute',
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
    },
    modalView: {
        // backgroundColor: Color.BLACK,
        flex: 1,
        justifyContent: 'space-between',

        height: ThemeUtils.relativeHeight(100),
        width: ThemeUtils.relativeWidth(100),
    },
    vwHeaderIcons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 5,
    },
    vwHeaderRightIcons: {
        flexDirection: 'row',
        width: ThemeUtils.relativeWidth(60),
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    closeIconStyle: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        alignSelf: 'flex-start',
    },
    closeIcon: {},
    vwTextContainer: {
        height: ThemeUtils.relativeHeight(8),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 5,
    },
    sendIconStyle: {
        backgroundColor: Color.PRIMARY,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        paddingVertical: 15,
        paddingEnd: 15,
        paddingStart: 20,
    },
    sendIcon: {},
    inputStyle: {
        backgroundColor: Color.INPUT_BACKGROUND,
        height: ThemeUtils.relativeHeight(8),
        width: ThemeUtils.relativeWidth(80),
        paddingVertical: 10,
        paddingHorizontal: 20,
        color: Color.WHITE,
        borderRadius: 30,
        fontSize: ThemeUtils.fontNormal,
    },
    Tstyle: {
        fontSize: ThemeUtils.responsiveFontSize(30),
        fontFamily: ThemeUtils.FontStyle.regular,
        color: Color.WHITE,
        backgroundColor: 'pink',
    },
    vwFilterLabel: {
        alignItems: 'center',
        margin: 20,
    },
    filterFlatListStyle: {
        backgroundColor: Color.INPUT_BACKGROUND,
        paddingVertical: 5,

        alignItems: 'flex-end',
    },
    filterImageStyle: {
        height: ThemeUtils.relativeWidth(30),
        width: ThemeUtils.relativeWidth(20),
        margin: 5,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    selectedFilterImageStyle: {
        height: ThemeUtils.relativeWidth(34),
        width: ThemeUtils.relativeWidth(22),
        margin: 5,
    },
    vwRenderFilter: {
        alignItems: 'center',
        justifyContent: 'flex-end',
    },
    checkMarkIcon: {
        backgroundColor: Color.PRIMARY,
        borderRadius: ThemeUtils.relativeWidth(7),
        borderWidth: 2,
        borderColor: Color.INPUT_BACKGROUND,
        position: 'absolute',
        top: 10,
        right: 10,
        aspectRatio: 1,
    },
});
