/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useRef} from 'react';
import {useIsFocused} from '@react-navigation/native';

import {View, FlatList, Image, SafeAreaView, ScrollView} from 'react-native';

import {styles} from './styles';

// Custom Component
import {Label, Ripple, CustomModal} from 'src/component';

// images
import GROUP_PLACEHOLDER from 'src/assets/images/group_placeholder.png';

// Utils
import {CommonStyle, Strings, Color, ThemeUtils} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import storage from '@react-native-firebase/storage';
import moment from 'moment';
import {TouchableOpacity} from 'react-native-gesture-handler';

const GroupDetails = (props) => {
    const {chatKey} = props.route?.params;
    const user = auth().currentUser;
    const userId = auth().currentUser.uid;
    const [groupImageUrl, setGroupImageUrl] = useState(null);
    const [groupInfo, setGroupInfo] = useState(null);
    const [participants, setParticipants] = useState([]);
    const listRef = useRef();
    const [modalVisible, setModalVisible] = useState(false);
    const [loggedUserInfo, setLoggedUserInfo] = useState(null);
    const isFocused = useIsFocused();
    const [visibleParticipants, setVisibleParticipants] = useState([]);

    /*  Life-cycles Methods */
    useEffect(async () => {
        if (isFocused) {
            database()
                .ref(`allUsers/${user?.uid}`)
                .once('value')
                .then((snapshot) => {
                    setLoggedUserInfo(snapshot.val());
                });

            await storage()
                .ref(chatKey)
                .getDownloadURL()
                .then(onResolve, onError);

            database()
                .ref(`groups/${userId}/${chatKey}`)
                .once('value')
                .then((snapshot) => {
                    setGroupInfo(snapshot.val());
                    setParticipants(snapshot.val().participants);
                });
        }
        return () => {};
    }, [isFocused]);

    useEffect(() => {
        if (loggedUserInfo && participants) {
            setVisibleParticipants([
                {
                    uid: userId,
                    name: 'You',
                    imageUrl: loggedUserInfo?.profileImageUrl,
                },
                ...participants,
            ]);
        }
        return () => {};
    }, [loggedUserInfo, participants]);

    /*  UI Events Methods   */

    const onResolve = (url) => {
        setGroupImageUrl(url);
    };

    const onError = (error) => {
        setGroupImageUrl(null);
    };
    const deleteGroup = async () => {
        await database().ref(`chats/${chatKey}`).remove();
        await storage().ref(`${chatKey}`).delete();
        participants?.map(async (item) => {
            await database().ref(`groups/${item?.uid}/${chatKey}`).remove();
        });
        await database().ref(`groups/${userId}/${chatKey}`).remove();
        props.navigation.navigate(Routes.Home);
    };

    const handleEditGroupName = () => {
        props.navigation.navigate(Routes.GroupName, {
            groupName: groupInfo?.groupName,
            chatKey,
        });
    };

    const goToEditGroupImage = () => {
        props.navigation.navigate(Routes.GroupImage, {
            chatKey,
        });
    };
    const handleOnCancelPress = () => {
        setModalVisible(false);
    };

    const handleOnDeletePress = () => {
        setModalVisible(false);
        deleteGroup();
    };

    const handleDeleteGroup = async () => {
        setModalVisible(true);
    };

    /* Custom-Component Sub-render Method */

    const RenderItemCompo = ({itemData}) => {
        return (
            <Ripple style={styles.rplStyle}>
                <Image
                    source={
                        itemData?.imageUrl
                            ? {uri: itemData.imageUrl}
                            : require('src/assets/images/user_placeholder_image.png')
                    }
                    style={styles.userProfileStyle}
                />
                <View style={styles.vwNameAndTime}>
                    <Label style={styles.nameStyle} color={Color.BLACK}>
                        {itemData?.name}
                    </Label>
                </View>
            </Ripple>
        );
    };

    return (
        <SafeAreaView style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <TouchableOpacity onPress={() => goToEditGroupImage()}>
                        <Image
                            source={
                                groupImageUrl
                                    ? {
                                          uri: groupImageUrl,
                                      }
                                    : GROUP_PLACEHOLDER
                            }
                            style={styles.groupImageStyle}
                        />
                    </TouchableOpacity>
                    <View style={styles.vwGroupDetail}>
                        <View style={styles.vwLabels}>
                            <Label
                                numberOfLines={1}
                                ellipsizeMode={'tail'}
                                bolder
                                xlarge
                                color={Color.WHITE}>
                                {groupInfo?.groupName}
                            </Label>
                            <Label
                                numberOfLines={1}
                                ellipsizeMode={'tail'}
                                xsmall
                                color={Color.WHITE}>
                                {Strings.createdOn +
                                    ' ' +
                                    moment(groupInfo?.createdAt).format('L')}
                            </Label>
                        </View>

                        <Ripple
                            rippleContainerBorderRadius={ThemeUtils.relativeWidth(
                                20,
                            )}
                            onPress={() => handleEditGroupName()}
                            style={styles.pencilIconStyle}>
                            <IonIcon
                                name="pencil-outline"
                                style={styles.pencilIcon}
                                color={Color.WHITE}
                                size={ThemeUtils.relativeWidth(7)}
                            />
                        </Ripple>
                    </View>
                    {modalVisible && (
                        <CustomModal
                            modalVisible={modalVisible}
                            labels
                            label1={Strings.deleteGroup}
                            label2={Strings.deleteGroupConfirmation}
                            button1Text={Strings.cancel}
                            button2Text={Strings.delete}
                            button1Press={() => handleOnCancelPress()}
                            button2Press={() => handleOnDeletePress()}
                            onRequestClose={() => setModalVisible(false)}
                        />
                    )}
                    <View style={{top: -ThemeUtils.relativeHeight(8.5)}}>
                        <Label ms={17} mt={15} color={Color.PARTICIPANT_COLOR}>
                            {visibleParticipants?.length + ' participants'}
                        </Label>
                        <FlatList
                            ref={listRef}
                            data={visibleParticipants}
                            style={[styles.flatListStyle]}
                            renderItem={({item}) => (
                                <RenderItemCompo itemData={item} />
                            )}
                            keyExtractor={(item, index) => index.toString()}
                            onContentSizeChange={() =>
                                listRef.current.scrollToEnd()
                            }
                        />
                    </View>
                    <Ripple
                        style={styles.lblDeleteGroup}
                        onPress={() => handleDeleteGroup()}>
                        <Label bolder color={Color.ERROR_COLOR}>
                            {Strings.deleteGroup}
                        </Label>
                    </Ripple>
                </ScrollView>
            </View>
        </SafeAreaView>
    );
};

export default GroupDetails;
