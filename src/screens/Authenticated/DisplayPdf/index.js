/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';

import {View, Image} from 'react-native';

import {styles} from './styles';

// third-party
import Pdf from 'react-native-pdf';
// import RNFetchBlob from 'rn-fetch-blob';
import FileViewer from 'react-native-file-viewer';

const DisplayPdf = (props) => {
    const {uri, name} = props?.route?.params;
    console.log('uri :: ', uri);

    //https://github.com/RameshMF/Java-Free-Resources-By-JavaGuides/raw/master/OOPS%20Concepts%20in%20Java%20PDF%20Download.pdf
    const displayUri = {
        uri: uri,
        cache: true,
    };

    return (
        <View style={styles.container}>
            <Pdf
                // ref={(pdf) => {
                //     this.pdf = pdf;
                // }}
                source={displayUri}
                style={styles.pdf}
                onError={(error) => {
                    console.log(error);
                }}
            />
        </View>
    );
};

export default DisplayPdf;
