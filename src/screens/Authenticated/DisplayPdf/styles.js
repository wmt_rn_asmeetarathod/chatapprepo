import {StyleSheet} from 'react-native';
import {ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    imageStyle: {
        height: ThemeUtils.relativeWidth(100),
        width: ThemeUtils.relativeWidth(100),
        resizeMode: 'cover',
    },
    pdf: {
        flex: 1,
        width: ThemeUtils.relativeWidth(100),
        height: ThemeUtils.relativeHeight(100),
    },
});
