/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useLayoutEffect} from 'react';
import {View, Image, SafeAreaView} from 'react-native';
import {styles} from './styles';

// Utils
import {CommonStyle} from 'src/utils';

// third-party
import database from '@react-native-firebase/database';

const UserDetails = (props) => {
    const {otherUserUid} = props?.route?.params;
    const [otherUserInfo, setOtherUserInfo] = useState(null);

    /*  Life-cycles Methods */
    useEffect(() => {
        database()
            .ref(`allUsers/${otherUserUid}`)
            .once('value')
            .then((snapshot) => {
                setOtherUserInfo(snapshot.val());
            });

        return () => {};
    }, []);

    useLayoutEffect(() => {
        props.navigation.setOptions({
            title: otherUserInfo?.firstName + ' ' + otherUserInfo?.lastName,
        });
    }, [otherUserInfo]);

    return (
        <SafeAreaView style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <Image
                    source={
                        otherUserInfo?.profileImageUrl
                            ? {
                                  uri: otherUserInfo?.profileImageUrl,
                              }
                            : require('src/assets/images/user_placeholder_image.png')
                    }
                    style={styles.groupImageStyle}
                />
            </View>
        </SafeAreaView>
    );
};

export default UserDetails;
