import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    groupImageStyle: {
        height: ThemeUtils.relativeWidth(100),
        width: ThemeUtils.relativeWidth(100),
        resizeMode: 'cover',
    },
    vwGroupDetail: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: Color.DARK_LIGHT_BLACK,
        top: -53,
    },
    vwLabels: {
        width: ThemeUtils.relativeWidth(80),
    },
    pencilIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        width: ThemeUtils.relativeWidth(8),
        flex: 0.8,
    },
    pencilIcon: {
        height: ThemeUtils.relativeWidth(7),
        width: ThemeUtils.relativeWidth(7),
        borderRadius: ThemeUtils.relativeWidth(3.5),
    },
    rplStyle: {
        backgroundColor: Color.WHITE,
        padding: 17,
        borderBottomColor: Color.TEXT_PLACEHOLDER,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    userProfileStyle: {
        width: ThemeUtils.relativeWidth(15),
        height: ThemeUtils.relativeWidth(15),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(7.5),
        marginEnd: 20,
    },
    vwNameAndTime: {
        flexDirection: 'row',
    },
    nameStyle: {
        flex: 1,
        marginEnd: 65,
    },
    lblDeleteGroup: {
        justifyContent: 'center',
        paddingStart: ThemeUtils.relativeWidth(4),
        paddingVertical: ThemeUtils.relativeWidth(3),
        backgroundColor: Color.LIGHT_GRAY,
    },
});
