/* eslint-disable  */

import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 10,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    flatListStyle: {
        flex: 1,
    },
    myStyle: {
        backgroundColor: Color.PRIMARY_DARK,
        color: Color.WHITE,
        alignSelf: 'flex-end',
        paddingVertical: 10,
        elevation: 5,
        borderTopLeftRadius: 17,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 5,
        paddingHorizontal: 33,
    },
    otherStyle: {
        backgroundColor: Color.MANDARIAN_ORANGE,
        color: Color.WHITE,
        borderRadius: 30,
        alignSelf: 'flex-start',
        elevation: 5,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 17,
        borderBottomLeftRadius: 17,
        borderBottomRightRadius: 17,
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    content: {
        // paddingVertical: 10,
        // paddingHorizontal: 10,
    },
    myTimeStyle: {
        alignSelf: 'flex-end',
    },
    otherTimeStyle: {
        alignSelf: 'flex-start',
    },
    itemStyle: {
        padding: 10,
    },
    sendContainerStyle: {
        position: 'absolute',
        bottom: 0,
        left: 10,
        right: 10,
        maxHeight: ThemeUtils.relativeRealHeight(12),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 7,
    },
    contentStyle: {
        flex: 1,
        color: Color.WHITE,
        paddingStart: 20,
        backgroundColor: Color.PRIMARY_DARK,
        borderRadius: 30,
        maxHeight: ThemeUtils.relativeRealHeight(12),
        marginVertical: 5,
    },
    sendLabelStylewithColor: {
        color: Color.PRIMARY_DARK,
        alignSelf: 'center',
        marginStart: 5,
    },
    sendLabelStylewithoutColor: {
        color: Color.LIGHT_GRAY,
        alignSelf: 'center',
        marginStart: 5,
    },
    dayStyle: {
        alignItems: 'center',
    },
    headerLeftStyle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    userProfileStyle: {
        width: ThemeUtils.relativeWidth(10),
        height: ThemeUtils.relativeWidth(10),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(5),
        // marginEnd : 20,
    },
    directionRow: {
        flexDirection: 'row',
    },
    marginStart: {
        marginStart: 10,
    },
});
