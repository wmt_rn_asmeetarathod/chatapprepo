/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useRef, useLayoutEffect} from 'react';
import {useIsFocused} from '@react-navigation/native';
import {HeaderBackButton} from '@react-navigation/stack';

import {styles} from './styles';

import {FlatList, View, Image, BackHandler} from 'react-native';

// custom component
import {Label, Ripple} from 'src/component';

// images
import GROUP_PLACEHOLDER from 'src/assets/images/group_placeholder.png';

// utils
import {CommonStyle, Strings, ThemeUtils, Color} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import {AutoGrowingTextInput} from 'react-native-autogrow-textinput';
import storage from '@react-native-firebase/storage';
import moment from 'moment';

const GroupChat = (props) => {
    const {chatKey} = props?.route?.params;
    const user = auth().currentUser;
    const [participants, setParticipants] = useState([]);
    const [groupInfo, setGroupInfo] = useState(null);
    const [chats, setChats] = useState([]);
    const [content, setContent] = useState('');
    const [readError, setReadError] = useState(null);
    const listRef = useRef();
    const [sendContainerHeight, setSendContainerHeight] = useState(
        ThemeUtils.relativeRealHeight(8.5),
    );
    const [containsText, setContainsText] = useState(false);
    const [loggedUserName, setLoggedUserName] = useState(null);
    const [groupImageUrl, setGroupImageUrl] = useState(null);
    const isFocused = useIsFocused();

    /*  Life-cycles Methods */
    useEffect(async () => {
        if (isFocused) {
            await storage()
                .ref(chatKey)
                .getDownloadURL()
                .then((url) => setGroupImageUrl(url));

            database()
                .ref(`allUsers/${user?.uid}`)
                .once('value')
                .then((snapshot) => {
                    setLoggedUserName(
                        snapshot.val().firstName +
                            ' ' +
                            snapshot.val().lastName,
                    );
                });
        }
        return () => {};
    }, [isFocused]);

    useEffect(() => {
        database()
            .ref(`groups/${user?.uid}/${chatKey}`)
            .on('value', (snapshot) => {
                setGroupInfo(snapshot.val());
                setParticipants(snapshot.val().participants);
            });
    }, []);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
        return () => {
            BackHandler.removeEventListener(
                'hardwareBackPress',
                backButtonHandler,
            );
        };
    }, []);

    useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: (props) => {
                return (
                    <View style={styles.headerLeftStyle}>
                        <HeaderBackButton
                            {...props}
                            onPress={() => backButtonHandler()}
                        />
                        <RenderProfileImage {...props} />
                    </View>
                );
            },
        });
    }, [groupInfo, participants, groupImageUrl]);

    useEffect(() => {
        let mounted = true;
        if (user) {
            try {
                // retrieve chats from node

                if (mounted) {
                    database()
                        .ref(`chats/${chatKey}`)
                        .on('value', (snapshot) => {
                            let chats = [];
                            snapshot.forEach((snap) => {
                                chats.push(snap.val());
                            });
                            setChats(chats);
                            generateGroupedChat(chats);
                        });
                }
            } catch (error) {
                setReadError(error.message);
            }
        }
        return () => (mounted = false);
    }, []);

    /*  UI Events Methods   */

    // const checkUserExist = () => {
    //     if (user) {
    //         database()
    //             .ref(`allUsers`)
    //             .once('value')
    //             .then((snap) => {
    //                 if (userId === user.uid && snap.hasChild(`${otherUid}`)) {
    //                     database()
    //                         .ref(`allUsers/${userId}`)
    //                         .once('value')
    //                         .then((snap) => {
    //                             setSenderName(
    //                                 `${snap.val().firstName} ${
    //                                     snap.val().lastName
    //                                 }`,
    //                             );

    //                             return `${snap.val().firstName} ${
    //                                 snap.val().lastName
    //                             }`;
    //                         });
    //                     database()
    //                         .ref(`allUsers/${otherUid}`)
    //                         .once('value')
    //                         .then((snap) => {
    //                             setReceiverName(
    //                                 `${snap.val().firstName} ${
    //                                     snap.val().lastName
    //                                 }`,
    //                             );
    //                             if (snap.val().profileImageUrl) {
    //                                 setImageUri(snap.val().profileImageUrl);
    //                             }
    //                             return `${snap.val().firstName} ${
    //                                 snap.val().lastName
    //                             }`;
    //                         });
    //                 } else {
    //                     ToastAndroid.show('User NOT Found', ToastAndroid.LONG);
    //                     props.navigation.dispatch(
    //                         CommonActions.reset({
    //                             index: 0,
    //                             routes: [{name: Routes.Home}],
    //                         }),
    //                     );
    //                 }
    //             });
    //     } else {
    //         props.navigation.dispatch(
    //             CommonActions.reset({
    //                 index: 0,
    //                 routes: [{name: Routes.UnAuthenticated}],
    //             }),
    //         );
    //     }
    //     return true;
    // };

    // backhandler
    const backButtonHandler = () => {
        props.navigation.navigate(Routes.Home);
        return true;
    };

    const onSend = () => {
        if (content.trim() !== '') {
            database().ref(`groups/${user?.uid}/${chatKey}`).update({
                content: content.trim(),
                timestamp: Date.now(),
                senderUid: user?.uid,
                senderName: loggedUserName,
            });
            participants?.map((item) => {
                database().ref(`groups/${item?.uid}/${chatKey}`).update({
                    content: content.trim(),
                    timestamp: Date.now(),
                    senderUid: user?.uid,
                    senderName: loggedUserName,
                });
            });
            database().ref(`chats/${chatKey}`).push({
                content: content.trim(),
                timestamp: Date.now(),
                uid: user?.uid,
                senderUid: user?.uid,
                senderName: loggedUserName,
            });

            setContent('');
            setContainsText(false);
        }
    };

    // To get Time Format
    const getTimeFormat = (timeStamp) => {
        let d = new Date(timeStamp);
        let hours = d.getHours();
        let minutes = d.getMinutes();
        let meridiem = hours >= 12 ? 'PM' : 'AM';

        // Find current hour in AM-PM Format
        hours = hours % 12;

        // To display "0" as "12"
        hours = hours ? hours : 12;
        hours = hours < 10 ? '0' + hours : hours;
        minutes = minutes < 10 ? '0' + minutes : minutes;
        let formattedTime = `${hours}:${minutes} ${meridiem}`;
        return formattedTime;
    };

    const handleContent = (text) => {
        setContent(text);
        if (text.trim() != '') {
            setContainsText(true);
        } else {
            setContainsText(false);
        }
    };

    const getYesterDayDate = () => {
        let yesterDayDate = new Date();
        yesterDayDate.setDate(yesterDayDate.getDate() - 1);
        return yesterDayDate;
    };

    // To Get Tag for Display date,today and yesterday
    const getTag = (date) => {
        let displayTag = '';

        if (
            moment(date).format('LL') ===
            moment(getYesterDayDate()).format('LL')
        ) {
            displayTag = Strings.yesterday;
        } else if (
            moment(date).format('LL') === moment(new Date()).format('LL')
        ) {
            displayTag = Strings.today;
        } else {
            displayTag = moment(date).format('LL');
        }
        return displayTag;
    };

    const generateGroupedChat = (chatArr) => {
        let copyChats = chatArr.map((chat, index, arr) => {
            let objChat = {...chat};
            if (index === 0) {
                objChat = {
                    type: 'dayTag',
                    tag: getTag(chat.timestamp),
                    ...objChat,
                };
            }

            if (index > 0 && index < chatArr.length) {
                if (
                    moment(chat.timestamp).format('LL') !==
                    moment(arr[index - 1].timestamp).format('LL')
                ) {
                    objChat = {
                        type: 'dayTag',
                        tag: getTag(chat.timestamp),
                        ...objChat,
                    };
                }
            }
            return objChat;
        });
        setChats(copyChats);
    };

    const goToGroupDetails = () => {
        props.navigation.navigate(Routes.GroupDetails, {
            chatKey,
        });
    };
    /* Custom-Component Sub-render Method */

    const RenderProfileImage = () => {
        return (
            <Ripple onPress={() => goToGroupDetails()}>
                <View style={styles.directionRow}>
                    <Image
                        source={
                            groupImageUrl
                                ? {uri: groupImageUrl}
                                : GROUP_PLACEHOLDER
                        }
                        style={styles.userProfileStyle}
                    />
                    <View style={styles.marginStart}>
                        <Label normal color={Color.WHITE} bolder>
                            {groupInfo?.groupName}
                        </Label>
                        <View style={styles.directionRow}>
                            <Label xsmall color={Color.MEMBER_NAME_COLOR}>
                                {Strings.you + ' , '}
                            </Label>
                            {participants?.map((item, index) =>
                                index === participants?.length - 1 ? (
                                    <Label
                                        key={item?.uid}
                                        xsmall
                                        color={Color.MEMBER_NAME_COLOR}>
                                        {item?.name}
                                    </Label>
                                ) : (
                                    <Label
                                        key={item?.uid}
                                        xsmall
                                        color={Color.MEMBER_NAME_COLOR}>
                                        {item?.name + ' , '}
                                    </Label>
                                ),
                            )}
                        </View>
                    </View>
                </View>
            </Ripple>
        );
    };

    const RenderItemCompo = ({chat}) => {
        return (
            <>
                {chat.type && (
                    <View style={styles.dayStyle}>
                        <Label color={Color.DARK_LIGHT_BLACK} xsmall>
                            {chat.tag}
                        </Label>
                    </View>
                )}
                <View style={styles.itemStyle}>
                    <Ripple
                        key={chat.timestamp}
                        style={
                            chat.uid === user?.uid
                                ? styles.myStyle
                                : styles.otherStyle
                        }
                        rippleContainerBorderRadius={20}>
                        {chat.uid !== user?.uid && (
                            <Label color={Color.SENDER_TEXT_COLOR} bolder small>
                                {chat?.senderName}
                            </Label>
                        )}
                        <Label style={styles.content} color={Color.WHITE} small>
                            {chat?.content}
                        </Label>
                    </Ripple>
                    <Label
                        xsmall
                        style={
                            chat.uid === user?.uid
                                ? styles.myTimeStyle
                                : styles.otherTimeStyle
                        }
                        color={Color.DARK_LIGHT_BLACK}>
                        {getTimeFormat(chat.timestamp)}
                    </Label>
                </View>
            </>
        );
    };

    return (
        <>
            <View style={CommonStyle.master_full_flex}>
                <View style={styles.container}>
                    {chats.length > 0 && (
                        <FlatList
                            ref={listRef}
                            data={chats}
                            style={[
                                styles.flatListStyle,
                                {
                                    marginBottom: sendContainerHeight + 5,
                                },
                            ]}
                            renderItem={({item}) => (
                                <RenderItemCompo chat={item} />
                            )}
                            keyExtractor={(item, index) => index.toString()}
                            // initialScrollIndex={chats.length - 1}
                            onContentSizeChange={() =>
                                listRef.current.scrollToEnd()
                            }
                        />
                    )}
                    <View
                        style={styles.sendContainerStyle}
                        onLayout={(e) =>
                            setSendContainerHeight(e.nativeEvent.layout.height)
                        }>
                        <AutoGrowingTextInput
                            style={styles.contentStyle}
                            value={content}
                            onChangeText={(text) => handleContent(text)}
                            placeholder={Strings.sendAMessage}
                        />
                        <Label
                            bolder
                            style={
                                containsText
                                    ? styles.sendLabelStylewithColor
                                    : styles.sendLabelStylewithoutColor
                            }
                            onPress={() => {
                                containsText && onSend();
                            }}>
                            {Strings.send}
                        </Label>
                    </View>
                </View>
            </View>
        </>
    );
};

export default GroupChat;
