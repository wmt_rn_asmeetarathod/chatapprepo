/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect} from 'react';
import {View, TextInput} from 'react-native';
import {styles} from './styles';

// Custom Component
import {Label, RoundButton} from 'src/component';

// Utils
import {CommonStyle, Strings, Color, ThemeUtils} from 'src/utils';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import {Controller, useForm} from 'react-hook-form';

const GroupName = (props) => {
    const {
        control,
        clearErrors,
        formState: {errors},
        handleSubmit,
    } = useForm();
    const {chatKey, groupName} = props.route?.params;
    const userId = auth().currentUser.uid;
    const [participants, setParticipants] = useState([]);

    /*  Life-cycles Methods */
    useEffect(async () => {
        database()
            .ref(`groups/${userId}/${chatKey}`)
            .once('value')
            .then((snapshot) => {
                setParticipants(snapshot.val().participants);
            });

        return () => {};
    }, []);

    const updateName = (name) => {
        participants?.map((item) => {
            database().ref(`groups/${item?.uid}/${chatKey}`).update({
                groupName: name,
            });
        });
        database()
            .ref(`groups/${userId}/${chatKey}`)
            .update({
                groupName: name,
            })
            .then(() => {
                props.navigation.goBack();
            });
    };

    const updateGroupName = (data) => {
        if (data?.groupName !== '') {
            data?.groupName === groupName
                ? props.navigation.goBack()
                : updateName(data?.groupName);
        }
    };

    const goBack = () => {
        props.navigation.goBack();
    };

    return (
        <View style={styles.container}>
            <Controller
                control={control}
                render={({onChange, onBlur, value}) => (
                    <View>
                        <TextInput
                            onFocus={() => clearErrors('groupName')}
                            value={value}
                            placeholder={Strings.enterGroupName}
                            onChangeText={(e) => onChange(e)}
                            returnKeyType="done"
                            style={styles.inputStyle}
                            onSubmitEditing={handleSubmit(updateGroupName)}
                        />
                        {errors.groupName?.message !== '' && (
                            <Label xsmall color={Color.ERROR_COLOR} ms={8}>
                                {errors.groupName?.message}
                            </Label>
                        )}
                    </View>
                )}
                name={'groupName'}
                defaultValue={groupName}
                rules={{
                    required: 'Please enter Group name',
                }}
            />
            <View style={styles.vwBtns}>
                <RoundButton
                    click={() => goBack()}
                    width={ThemeUtils.relativeRealWidth(46)}
                    mt={ThemeUtils.relativeRealHeight(4)}
                    textColor={Color.WHITE}
                    style={CommonStyle.full_flex}>
                    {Strings.cancel}
                </RoundButton>
                <RoundButton
                    click={handleSubmit(updateGroupName)}
                    width={ThemeUtils.relativeRealWidth(46)}
                    mt={ThemeUtils.relativeRealHeight(4)}
                    textColor={Color.WHITE}
                    style={CommonStyle.full_flex}>
                    {Strings.ok}
                </RoundButton>
            </View>
        </View>
    );
};

export default GroupName;
