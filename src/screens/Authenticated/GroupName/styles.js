import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'column',
    },
    vwBtns: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingVertical: 10,
    },
    inputStyle: {
        backgroundColor: Color.WHITE,
        fontSize: ThemeUtils.fontNormal,
        borderBottomWidth: 1,
        marginHorizontal: 8,
        marginBottom: 5,
    },
});
