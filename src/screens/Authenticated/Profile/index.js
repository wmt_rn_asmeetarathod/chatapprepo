/* eslint-disable  */
import React,
{
    useState,
    useEffect,
    useLayoutEffect,

} from 'react';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import storage from '@react-native-firebase/storage';
import ProgressBar from 'react-native-progress/Bar';
import ProgressCircle from 'react-native-progress-circle';
import PushNotification,
{
    Importance
} from "react-native-push-notification";
import { useIsFocused } from '@react-navigation/native';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import {
    View,
    ScrollView,
    FlatList,
    ActivityIndicator,
    Modal,
    Alert,
    Text,
    ImageBackground,
    PermissionsAndroid
} from 'react-native';
import { styles } from './styles';
import {
    Label,
    MaterialTextInput,
    RoundButton,
    Ripple,
} from 'src/component';
import {
    CommonStyle,
    Strings,
    Color,
    PermissionUtils,
    ThemeUtils,
    showToast,
} from 'src/utils';
import Routes from 'src/router/Routes';

const Profile = (props) => {

    const [user, setUser] = useState(auth().currentUser);
    const [userData, setUserData] = useState();
    const [modalVisible, setModalVisible] = useState(false);
    const [imageUri, setImageUri] = useState();
    const [userEmail, setUserEmail] = useState(userData && userData.email);
    const [profileIndicator, setProfileIndicator] = useState(false);
    const [dataIndicator, setDataIndicator] = useState(false);
    const userPrevPassword = userData && userData.password;
    const [progress, setProgress] = useState(0);

    useEffect(() => {
        let mounted = true;

        if (mounted) {
            if (user) {
                try {
                    // retrieve chats from node

                    database().ref(`allUsers/${user.uid}`).on("value", snap => {
                        setUserData(snap.val());
                        setUserEmail(snap.val().email);
                    })

                } catch (error) {
                    console.log("useEffect (ProfileScreen) error ==>> ", error);
                }
            }
        }
        storage().ref(user.uid).getDownloadURL().then(url => setImageUri(url)).catch(() => setImageUri(null))

        return () => mounted = false;

    }, []);

    useEffect(() => {

    }, [imageUri, userData, progress]);

    useEffect(() => {

    }, [userEmail, dataIndicator]);

    useLayoutEffect(() => {
        props.navigation.setOptions({

            headerRight: () => {
                return (
                    <View style={styles.headerButtonsStyle}>
                        <Ripple
                            rippleContainerBorderRadius={50}
                            onPress={() => onPressSaveIcon()}
                            style={styles.checkMarkIconStyle}
                        >
                            <IonIcon name="checkmark-outline" style={styles.checkMarkIcon} color={Color.WHITE} size={30} />
                        </Ripple>
                    </View>
                )
            },
        })
    }
        , [userEmail]);

    const reAuthenticate = () => {
        const cred = auth.EmailAuthProvider.credential(user.email, userPrevPassword);
        return user.reauthenticateWithCredential(cred);
    }


    const onPressSaveIcon = async () => {
        setDataIndicator(true);

        if(!userEmail){
          await setUserEmail(user.email);
        }

        if (user.email === userEmail) {
            props.navigation.navigate(Routes.Home);
            setDataIndicator(false);
        }
        else {
            await reAuthenticate().then(() => {
                user.updateEmail(userEmail);
                auth().signInWithEmailAndPassword(userEmail, userPrevPassword)
                    .catch(error => {
                        setDataIndicator(false);
                        showToast(Strings.somethingWentWrong);
                    });
            })
                .then(() => {
                    database().ref(`allUsers/${user.uid}`).update({
                        email: userEmail,
                    }).then(() => {
                        setDataIndicator(false);
                        props.navigation.navigate(Routes.Home);
                        showToast(Strings.profileUpdated);
                    });
                })
                .catch(reAuthenticateError => {
                    setDataIndicator(false);
                    showToast(Strings.somethingWentWrong);
                });
        }

    }
    const uploadProgress = ratio => Math.round(ratio * 100);

    const storeImageToDB = (imageUrl) => {

        const userRef = storage().ref(user.uid);

        userRef.putFile(imageUrl.toString()).on('state_changed', snapshot => {

            const tempProgress = uploadProgress(
                snapshot.bytesTransferred / snapshot.totalBytes
            );

            switch (snapshot.state) {
                case 'running':
                    setProgress(tempProgress);
                    break;
                case 'success':
                    setProgress(tempProgress);
                    userRef.getDownloadURL().then(url => {
                        setImageUri(url);
                        setProfileIndicator(false);
                        setProgress(0);
                        database().ref(`allUsers/${user.uid}`).update({
                            profileImageUrl: url,
                        }
                        ).catch(() => {
                            setProfileIndicator(false);
                            setProgress(0);
                            showToast(Strings.cantUploadProfilePhoto)
                        })
                    });
                    break;

                default:
                    break;
            }
        })


    }

    const removeImageFromDB = async () => {
        setProgress(90);
        await storage().ref(user.uid).delete().then(() => {
            setProgress(100);
            setProfileIndicator(false);
            setProgress(0);
        });
    }

    const handleEditPicture = () => {
        setModalVisible(true);
    }

    const handleCapturePhoto = () => {
        setModalVisible(false);

        PermissionUtils.requestCameraPermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openCamera();
            }
        });
    }

    const openCamera = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
            saveToPhotos: true,
        };
        launchCamera(options, (response) => {

            if (response.didCancel) {

                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else if (response.errorCode == 'camera_unavailable') {

                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else if (response.errorCode == 'permission') {
                
                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else if (response.errorCode == 'others') {

                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else {
                setProfileIndicator(true);
                storeImageToDB(response.uri);
            }
        });
    }
    
    const handleChoosePhoto = () => {
        setModalVisible(false);

        /* For Storage Permission */
        PermissionUtils.requestStoragePermission().then((isGranted) => {
            //it will return true if permission granted otherwise false
            if (isGranted) {
                openGallary();
            }
        });

    }

    const openGallary = () => {
        let options = {
            mediaType: 'photo',
            quality: 1,
        };
        launchImageLibrary(options, (response) => {

            if (response.didCancel) {

                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else if (response.errorCode == 'camera_unavailable') {

                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else if (response.errorCode == 'permission') {

                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else if (response.errorCode == 'others') {
                
                setImageUri(imageUri);
                setProfileIndicator(false);
                setProgress(0);

            } else {

                setProfileIndicator(true);
                storeImageToDB(response.uri);
            }


        });
    }
    const handleRemovePhoto = () => {
        setModalVisible(false);
        setProfileIndicator(true);
        removeImageFromDB();
        setImageUri('');
    }

    const handleChangeEmail = (mail) => {
        setUserEmail(mail);
    }

    const onPressChangePassword = () => {
        props.navigation.navigate(Routes.ChangePassword)
    }

    return (
        <>
       
        <ScrollView contentContainerStyle={styles.mainContainer}>
            {
                modalVisible &&
                <View style={styles.modalContainer} onTouchEnd={()=>setModalVisible(false)}>
                    <Modal
                        animationType='slide'
                        transparent={true}
                        visible={modalVisible}
                        onRequestClose={() => setModalVisible(false)}                        
                    >
                        
                        <View style={styles.vwUpper}>
                            <View style={styles.modalView}>
                                <Label mb={10} onPress={handleCapturePhoto}>{Strings.capturePhoto}</Label>
                                <Label mb={10} onPress={handleChoosePhoto} >{Strings.chooseFromGallary}</Label>
                                {
                                    imageUri !== '' &&
                                    <Label onPress={handleRemovePhoto}>{Strings.removePhoto}</Label>
                                }

                            </View>
                        </View>
                    </Modal>
                 </View>
            }
            {
                profileIndicator
                    ?
                    <View style={styles.vwIndicator}>
                        <ProgressCircle
                            percent={progress}
                            radius={50}
                            borderWidth={8}
                            color={Color.PRIMARY_DARK}
                            shadowColor={Color.LIGHT_GRAY}
                            bgColor="#fff"
                        >
                            <Label>{progress}{'%'}</Label>
                        </ProgressCircle>
                    </View>
                    :
                    dataIndicator
                        ?
                        <View style={styles.vwDataIndicator}>
                            <ActivityIndicator color={Color.PRIMARY_DARK} size={'large'} />
                        </View>
                        :
                        <View style={styles.profileContainer} >

                            <ImageBackground
                                source={imageUri ? { uri: imageUri } :
                                    require('../../../assets/images/user_placeholder_image.png')}
                                style={styles.userStyle}
                                imageStyle={styles.userImageStyle}
                            >
                                <Ripple
                                    rippleContainerBorderRadius={50}
                                    onPress={() => handleEditPicture()}
                                    style={styles.editIconStyle}
                                >
                                    <IonIcon name="create-outline" style={styles.searchIcon} color={Color.WHITE} size={25} />
                                </Ripple>
                            </ImageBackground>

                            {
                                userData &&
                                <>
                                    <View style={styles.nameStyle}>
                                        <Label me={10}>{userData.firstName}</Label>
                                        <Label>{userData.lastName}</Label>

                                    </View>
                                    <View style={{ flexGrow: 1 }}>
                                        <MaterialTextInput
                                            // error={errors.password?.message}
                                            value={userEmail}
                                            label={'Email'}
                                            onChangeText={(text) => handleChangeEmail(text)}
                                        />

                                    </View>

                                </>
                            }
                            <View style={styles.btnVw}>
                                <RoundButton
                                    click={() => onPressChangePassword()}
                                    width={ThemeUtils.relativeRealWidth(80)}
                                    mt={ThemeUtils.relativeRealHeight(4)}
                                    textColor={Color.WHITE}
                                    style={styles.changePasswordBtnStyle}>

                                    {Strings.changePassword}
                                </RoundButton>
                            </View>

                        </View>

            }

        </ScrollView>
        </>
    )
}
export default Profile;