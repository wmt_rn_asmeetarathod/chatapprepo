/* eslint-disable  */
import { StyleSheet } from 'react-native';
import {
  Color,
  ThemeUtils
} from 'src/utils';

export const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    alignItems: 'center',
    // justifyContent: 'center',
    display: 'flex',
    padding: 20,
    backgroundColor: Color.WHITE,
  },
  userStyle: {
    width: ThemeUtils.relativeWidth(25),
    height: ThemeUtils.relativeWidth(25),
    margin: 30,
    alignSelf: 'center',
  },
  userImageStyle: {
    borderRadius: ThemeUtils.relativeWidth(12.5),
  },
  editIconStyle: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    backgroundColor: Color.PRIMARY_DARK,
    borderRadius: 50,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  nameStyle: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginBottom: 20,
  },
  modalContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    flex: 1,
    width: ThemeUtils.relativeWidth(90),
  },
  vwUpper: {
    display: 'flex',
    justifyContent: 'center',
    alignContent: 'center',
    flex: 1,
  },
  modalView: {
    margin: 30,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 25,
    alignItems: "center",
    justifyContent: 'center',
    alignSelf : 'center',
    width: ThemeUtils.relativeWidth(90),
    // alignContent:'center',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  profileContainer: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    // alignItems: 'center',
    display: 'flex',
    padding: 20,
    position: 'absolute',
    backgroundColor: Color.WHITE,
  },
  checkMarkIconStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  checkMarkIcon: {
    marginHorizontal: 10,
  },
  changePasswordBtnStyle: {
    alignSelf: 'center',
  },
  btnVw: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  vwIndicator: {
    flex: 1,
    flexGrow : 1,
    justifyContent : 'center',
    alignItems : 'center',
    alignSelf : 'center',
  },
  vwDataIndicator : {
    flex: 1,
    flexGrow : 1,
    justifyContent : 'center',
    alignItems : 'center',
    alignSelf : 'center',
  },
});
