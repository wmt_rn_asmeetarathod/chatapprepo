import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    searchIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        padding: ThemeUtils.relativeWidth(2),
        aspectRatio: 1,
    },
    searchIcon: {
        // marginStart: 10,
        // marginEnd: 10,
    },
    headerButtonsStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: ThemeUtils.relativeWidth(4),
    },
    headerLeftStyle: {
        marginStart: ThemeUtils.relativeWidth(4),
    },
    rplStyle: {
        backgroundColor: Color.WHITE,
        padding: 17,
        borderBottomColor: Color.TEXT_PLACEHOLDER,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    userProfileStyle: {
        width: ThemeUtils.relativeWidth(15),
        height: ThemeUtils.relativeWidth(15),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(7.5),
        marginEnd: 20,
    },
    nameStyle: {
        flex: 1,
        marginEnd: 65,
    },
    vwNameAndTime: {
        flexDirection: 'row',
    },
    serchContainer: {
        flexDirection: 'row',
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        height: ThemeUtils.APPBAR_HEIGHT,
        borderColor: Color.LIGHT_GRAY,
        borderWidth: 1,
        borderRadius: 30,
        margin: 5,
    },
    vwInput: {
        flex: 1,
        height: ThemeUtils.APPBAR_HEIGHT,
        justifyContent: 'center',
    },
    inputStyle: {
        alignSelf: 'center',
        paddingStart: 10,
    },
});
