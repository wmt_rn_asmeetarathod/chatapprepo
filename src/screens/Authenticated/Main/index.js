/* eslint-disable react-hooks/exhaustive-deps */
import React, {useLayoutEffect, useState, useEffect} from 'react';
import {View, FlatList, Modal, Alert, Image} from 'react-native';
import {styles} from './styles';

// Custom Component
import {Label, MaterialTextInput, Ripple} from 'src/component';

// Utils
import {Strings, Color, CommonStyle, ThemeUtils} from 'src/utils';

//navigation
import Routes from 'src/router/Routes';
import MainTab from 'src/router/MainTab';

// third-party
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import database from '@react-native-firebase/database';
import auth from '@react-native-firebase/auth';

const Main = (props) => {
    const user = auth().currentUser;
    const [modalVisible, setModalVisible] = useState(false);
    const [searchUser, setSearchUser] = useState('');
    const [allUserList, setAllUserList] = useState([]);
    const [allUserListCopy, setAllUserListCopy] = useState([]);

    /*  Life-cycles Methods */

    // get All Users list
    useEffect(() => {
        let mounted = true;
        if (mounted) {
            database()
                .ref('allUsers')
                .on('value', (snapshot) => {
                    let userObj = [];
                    let profilePictures = [];
                    snapshot?.forEach((userSnap) => {
                        if (user.email != userSnap.val().email) {
                            userObj?.push(userSnap.val());
                            if (userSnap?.val()?.profileImageUrl) {
                                const profileKey = userSnap.key;
                                profilePictures[profileKey] =
                                    userSnap?.val().profileImageUrl;
                            }
                        }
                    });
                    setAllUserList(userObj);
                    setAllUserListCopy(userObj);
                });
        }
        return () => (mounted = false);
    }, []);

    // Update Header according to modal
    useLayoutEffect(() => {
        props.navigation.setOptions({
            headerLeft: () => {
                return (
                    <View style={styles.headerLeftStyle}>
                        {!modalVisible &&
                            renderHeaderIcon(
                                'person-outline',
                                ThemeUtils.relativeRealWidth(6),
                                onPressUserIcon,
                            )}
                    </View>
                );
            },

            headerRight: () => {
                return (
                    <>
                        {!modalVisible && (
                            <View style={styles.headerButtonsStyle}>
                                {renderHeaderIcon(
                                    'search-outline',
                                    ThemeUtils.relativeRealWidth(6),
                                    onPressSearchIcon,
                                )}
                                {renderHeaderIcon(
                                    'log-out-outline',
                                    ThemeUtils.relativeRealWidth(7),
                                    onPressLogout,
                                )}
                            </View>
                        )}
                    </>
                );
            },
            title: modalVisible ? null : 'WMT Chats',
        });
    }, [modalVisible]);

    /*  UI Events Methods   */

    const onPressUserIcon = () => {
        props.navigation.navigate(Routes.Profile);
    };

    const onPressLogout = () => {
        Alert.alert(Strings.logOutTitle, Strings.sureWantToLogOut, [
            {
                text: Strings.cancel,
                onPress: () => null,
                style: 'cancel',
            },
            {
                text: Strings.logOutCaps,
                onPress: () => userSignOut(),
                style: 'cancel',
            },
        ]);
    };

    const userSignOut = () => {
        auth()
            .signOut()
            .then(() => {
                props.navigation.navigate(Routes.UnAuthenticated);
            });
    };

    const searchFilterFunction = (text) => {
        setSearchUser(text);
        const filteredData = allUserListCopy.filter((item) => {
            const name = `${item.firstName.toLowerCase()} ${item.lastName.toLowerCase()}`;
            const textData = text.toLowerCase();

            return name.indexOf(textData) > -1;
        });
        setAllUserList(filteredData);
    };

    // Handle modal close press event
    const handleClose = () => {
        if (searchUser !== '') {
            setSearchUser('');
        } else {
            setModalVisible(false);
            setAllUserList(allUserListCopy);
        }
    };

    const handleOnPress = (uid, name, isGroup, chatKey) => {
        setModalVisible(false);
        isGroup
            ? props.navigation.navigate(Routes.GroupChat, {
                  chatKey,
              })
            : props.navigation.navigate(Routes.Chat, {
                  receiverId: uid,
                  userId: user.uid,
              });
    };

    const onPressSearchIcon = () => {
        setModalVisible(true);
    };

    const getName = (itemData) => {
        let name;
        if ('firstName' in itemData) {
            name = `${itemData?.firstName} ${itemData?.lastName}`;
        } else if ('groupName' in itemData) {
            name = itemData?.groupName;
        } else {
            name = itemData?.otherPersonName;
        }
        return name;
    };

    const getUid = (itemData) => {
        let uid;
        if ('firstName' in itemData) {
            uid = itemData?.uid;
        } else {
            uid = itemData?.otherPersonUid;
        }
        return uid;
    };

    /* Custom-Component Sub-render Method */

    const renderHeaderIcon = (name, size, onPress) => (
        <Ripple
            rippleContainerBorderRadius={50}
            onPress={() => onPress()}
            style={styles.searchIconStyle}>
            <IonIcon
                name={name}
                style={styles.searchIcon}
                color={Color.WHITE}
                size={size}
            />
        </Ripple>
    );

    const renderItemCompo = (itemData) => {
        const isGroup = 'groupName' in itemData;

        return (
            <>
                <Ripple
                    style={styles.rplStyle}
                    onPress={() =>
                        handleOnPress(
                            getUid(itemData),
                            getName(itemData),
                            isGroup,
                            itemData?.chatKey,
                        )
                    }>
                    <Image
                        source={
                            itemData.profileImageUrl
                                ? {uri: itemData.profileImageUrl}
                                : require('src/assets/images/user_placeholder_image.png')
                        }
                        style={styles.userProfileStyle}
                    />
                    <View style={styles.vwNameAndTime}>
                        <Label style={styles.nameStyle} color={Color.BLACK}>
                            {getName(itemData)}
                        </Label>
                    </View>
                </Ripple>
            </>
        );
    };

    const renderSearchModal = () => (
        <View>
            <Modal
                animationType="slide"
                transparent={false}
                visible={modalVisible}
                onRequestClose={() => setModalVisible(false)}>
                <View style={CommonStyle.master_full_flex}>
                    <View style={styles.serchContainer}>
                        <View style={styles.vwInput}>
                            <MaterialTextInput
                                value={searchUser}
                                style={styles.inputStyle}
                                onChangeText={(text) =>
                                    searchFilterFunction(text)
                                }
                                placeholder={Strings.search}
                                lineWidth={0}
                                activeLineWidth={0}
                                disabledLineWidth={0}
                                placeholderTextColor={Color.TEXT_PLACEHOLDER}
                            />
                        </View>
                        <Ripple
                            rippleContainerBorderRadius={50}
                            onPress={() => handleClose()}
                            style={styles.searchIconStyle}>
                            <IonIcon
                                name="close-outline"
                                style={styles.searchIcon}
                                color={Color.BLACK}
                                size={25}
                            />
                        </Ripple>
                    </View>
                    <FlatList
                        style={styles.allUserListStyle}
                        data={allUserList}
                        renderItem={({item}) => renderItemCompo(item)}
                        keyExtractor={(item) => item.uid.toString()}
                    />
                </View>
            </Modal>
        </View>
    );

    return (
        <>
            {renderSearchModal()}
            <MainTab />
        </>
    );
};

export default Main;
