/* eslint-disable react-hooks/exhaustive-deps */
import React, {useState, useEffect, useLayoutEffect} from 'react';
import {useIsFocused} from '@react-navigation/native';
import {
    View,
    FlatList,
    ActivityIndicator,
    Modal,
    Alert,
    Image,
} from 'react-native';
import {styles} from './styles';

// Custom Component
import {Label, MaterialTextInput, Ripple} from 'src/component';

// images
import GROUP_PLACEHOLDER from 'src/assets/images/group_placeholder.png';

// Utils
import {CommonStyle, Strings, Color, ThemeUtils} from 'src/utils';

// navigation
import Routes from 'src/router/Routes';

// third-party
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import IonIcon from 'react-native-vector-icons/dist/Ionicons';
import PushNotification, {Importance} from 'react-native-push-notification';

const Home = (props) => {
    const user = auth().currentUser;
    const [allUserList, setAllUserList] = useState([]);
    const [allUserListCopy, setAllUserListCopy] = useState([]);
    const [searchUser, setSearchUser] = useState('');
    const [visible, setVisible] = useState(true);
    const [visibleUserList, setVisibleUserList] = useState([]);
    const [profileImagesList, setProfileImagesList] = useState([]);
    const isFocused = useIsFocused();
    const [newGroupModalVisible, setNewGroupModalVisible] = useState(false);
    const [selectedUsers, setSelectedUsers] = useState([]);
    const [groupList, setGroupList] = useState([]);
    const [list, setList] = useState([]);

    /*  Life-cycles Methods */

    // for notification configuration
    useEffect(() => {
        PushNotification.createChannel(
            {
                channelId: '1',
                channelName: 'My channel',
                channelDescription:
                    'A channel to categorise your notifications',
                soundName: 'default',
                importance: Importance.HIGH,
                vibrate: true,
            },
            (created) => console.log(`createChannel returned '${created}'`),
        );

        PushNotification.configure({
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: (token) => {
                console.log('TOKEN:', token);
            },

            // (required) Called when a remote or local notification is opened or received
            onNotification: (notification) => {
                console.log('(HomeScreen) NOTIFICATION:', notification);
            },

            /*  (optional) Called when Registered Action is pressed and invokeApp is false,
             if true onNotification will be called (Android) */
            onAction: (notification) => {
                console.log('NOTIFICATION:', notification);
            },
            // Android only
            senderID: '748026823250',
            // iOS only
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },
            popInitialNotification: true,
            requestPermissions: true,
        });
    }, []);

    useEffect(() => {
        mergeList();
    }, [isFocused, visibleUserList, groupList]);

    // get All Users list
    useEffect(() => {
        let mounted = true;
        if (mounted) {
            database()
                .ref('allUsers')
                .on('value', (snapshot) => {
                    let userObj = [];
                    let profilePictures = [];
                    snapshot?.forEach((userSnap) => {
                        if (user.email != userSnap.val().email) {
                            userObj?.push(userSnap.val());
                            if (userSnap.val().profileImageUrl) {
                                const profileKey = userSnap.key;
                                profilePictures[profileKey] =
                                    userSnap?.val().profileImageUrl;
                            }
                        }
                    });
                    setAllUserList(userObj);
                    setAllUserListCopy(userObj);
                    setProfileImagesList(profilePictures);
                });
        }
        return () => (mounted = false);
    }, []);

    // Get One to One Chat User List and Groups List
    useEffect(() => {
        let mounted = true;
        try {
            database()
                .ref('groups')
                .on('value', (snap) => {
                    if (snap.hasChild(`${user?.uid}`)) {
                        database()
                            .ref(`groups/${user?.uid}`)
                            .on('value', (snap) => {
                                if (mounted) {
                                    if (snap.exists()) {
                                        let groupObj = [];
                                        snap.forEach(async (userSnap) => {
                                            groupObj.push(userSnap.val());
                                        });
                                        setGroupList(groupObj);
                                    } else {
                                    }
                                }
                            });
                    } else {
                    }
                });

            database()
                .ref(`users/${user.uid}`)
                .on('value', (snapshot) => {
                    if (mounted) {
                        if (snapshot.exists()) {
                            let userObj = [];
                            snapshot.forEach((userSnap) => {
                                userObj.push(userSnap.val());
                            });
                            setVisible(false);
                            setVisibleUserList(userObj);
                        } else {
                            setVisible(false);
                        }
                    }
                });
        } catch (error) {
            // setReadError(error.message);
        }
        return () => (mounted = false);
    }, [isFocused]);

    useEffect(() => {}, [newGroupModalVisible, selectedUsers]);

    /*  UI Events Methods   */

    // Merge and Sort Groups List and User List
    const mergeList = () => {
        let mergedList = [...groupList, ...visibleUserList];
        let sortedUserList = mergedList.sort((a, b) => {
            return b.timestamp - a.timestamp;
        });
        setList(sortedUserList);
    };

    const onPressUserIcon = () => {
        props.navigation.navigate(Routes.Profile);
    };

    const handleClose = () => {
        if (searchUser !== '') {
            setSearchUser('');
        } else {
            setNewGroupModalVisible(false);
            setAllUserList(allUserListCopy);
            setSelectedUsers([]);
        }
    };

    const handleCloseUser = (userUid) => {
        let filterArray = selectedUsers?.filter((user) => user.uid !== userUid);
        setSelectedUsers(filterArray);
    };

    const handleNewGroup = () => {
        setNewGroupModalVisible(true);
    };

    const goToCreateGroup = () => {
        if (selectedUsers?.length > 0) {
            setNewGroupModalVisible(false);
            props.navigation.navigate(Routes.CreateGroup, {
                selectedUsers,
            });
            setSelectedUsers([]);
        } else {
            Alert.alert('Select User', 'Please select at least one user');
        }
    };

    const onPressLogout = () => {
        Alert.alert(Strings.logOutTitle, Strings.sureWantToLogOut, [
            {
                text: Strings.cancel,
                onPress: () => null,
                style: 'cancel',
            },
            {
                text: Strings.logOutCaps,
                onPress: () => userSignOut(),
                style: 'cancel',
            },
        ]);
    };

    const userSignOut = () => {
        auth()
            .signOut()
            .then(() => {
                props.navigation.navigate(Routes.UnAuthenticated);
            });
    };

    const selectUser = (uid, name, imageUrl) => {
        if (selectedUsers.filter((user) => user.uid === uid).length > 0) {
            let filterArray = selectedUsers?.filter((user) => user.uid !== uid);
            setSelectedUsers(filterArray);
        } else {
            selectedUsers?.length > 0
                ? setSelectedUsers([
                      ...selectedUsers,
                      {
                          uid,
                          name,
                          imageUrl,
                      },
                  ])
                : setSelectedUsers([
                      {
                          uid,
                          name,
                          imageUrl,
                      },
                  ]);
        }
    };

    const searchFilterFunction = (text) => {
        setSearchUser(text);
        const filteredData = allUserListCopy.filter((item) => {
            const name = `${item.firstName.toLowerCase()} ${item.lastName.toLowerCase()}`;
            const textData = text.toLowerCase();

            return name.indexOf(textData) > -1;
        });
        setAllUserList(filteredData);
    };

    const handlePushNotification = () => {
        PushNotification.localNotification({
            channelId: '1',
            autoCancel: true,
            bigText:
                'This is local notification demo in React Native app. Only shown, when expanded.',
            subText: 'Local Notification Demo',
            title: 'Local Notification Title',
            message: 'Expand me to see more',
            vibrate: true,
            vibration: 300,
            playSound: true,
            soundName: 'default',
            actions: '["Accept", "Reject"]',
            smallIcon: '',
            color: Color.PRIMARY_DARK,
        });
        PushNotification.setApplicationIconBadgeNumber(3);
    };

    const handleOnPress = (uid, name, isGroup, chatKey) => {
        isGroup
            ? props.navigation.navigate(Routes.GroupChat, {
                  chatKey,
              })
            : props.navigation.navigate(Routes.Chat, {
                  receiverId: uid,
                  userId: user.uid,
              });
    };
    /* Custom-Component Sub-render Method */

    const RenderItemCompo = ({itemData}) => {
        const isGroup = 'groupName' in itemData;

        const getName = () => {
            let name;
            if ('firstName' in itemData) {
                name = `${itemData.firstName} ${itemData.lastName}`;
            } else if ('groupName' in itemData) {
                name = itemData?.groupName;
            } else {
                name = itemData.otherPersonName;
            }
            return name;
        };

        const getUid = () => {
            let uid;
            if ('firstName' in itemData) {
                uid = itemData.uid;
            } else {
                uid = itemData.otherPersonUid;
            }
            return uid;
        };

        // To get Time Format
        const getTimeFormat = (timeStamp) => {
            let d = new Date(timeStamp);
            let hours = d.getHours();
            let minutes = d.getMinutes();
            let meridiem = hours >= 12 ? 'PM' : 'AM';

            // Find current hour in AM-PM Format
            hours = hours % 12;

            // To display "0" as "12"
            hours = hours ? hours : 12;
            hours = hours < 10 ? '0' + hours : hours;
            minutes = minutes < 10 ? '0' + minutes : minutes;
            let formattedTime = `${hours}:${minutes} ${meridiem}`;
            return formattedTime;
        };

        // Format of Date
        const dateFormat = (timeStamp) => {
            let d = new Date(timeStamp);
            let date = d.getDate();
            let month = d.getMonth() + 1;
            let year = d.getFullYear();
            let formattedDate = date + '/' + month + '/' + year;
            return formattedDate;
        };

        // To Get Yesterday Date
        const getYesterDayDate = () => {
            let yesterDayDate = new Date();
            yesterDayDate.setDate(yesterDayDate.getDate() - 1);
            return yesterDayDate;
        };

        // To Get Tag
        const getTag = (date) => {
            let displayTag = '';
            if (dateFormat(date) === dateFormat(getYesterDayDate())) {
                displayTag = Strings.yesterday;
            } else if (dateFormat(date) === dateFormat(new Date())) {
                displayTag = getTimeFormat(date);
            } else {
                displayTag = dateFormat(date);
            }

            return displayTag;
        };

        return (
            <>
                {newGroupModalVisible ? (
                    <Ripple
                        style={
                            selectedUsers.filter(
                                (user) => user.uid === getUid(),
                            ).length > 0
                                ? styles.selectedStyle
                                : styles.rplStyle
                        }
                        onPress={() =>
                            selectUser(
                                getUid(),
                                getName(),
                                itemData?.profileImageUrl,
                            )
                        }>
                        <Image
                            source={
                                itemData.profileImageUrl
                                    ? {uri: itemData.profileImageUrl}
                                    : require('src/assets/images/user_placeholder_image.png')
                            }
                            style={styles.userProfileStyle}
                        />
                        <View style={styles.vwNameAndTime}>
                            <Label style={styles.nameStyle} color={Color.BLACK}>
                                {getName()}
                            </Label>
                        </View>
                    </Ripple>
                ) : (
                    <Ripple
                        style={styles.rplStyle}
                        onPress={() =>
                            handleOnPress(
                                getUid(),
                                getName(),
                                isGroup,
                                itemData?.chatKey,
                            )
                        }>
                        {isGroup ? (
                            <Image
                                source={
                                    itemData?.groupImageUrl
                                        ? {
                                              uri: itemData?.groupImageUrl,
                                          }
                                        : GROUP_PLACEHOLDER
                                }
                                style={styles.userProfileStyle}
                            />
                        ) : (
                            <Image
                                source={
                                    profileImagesList[getUid()]
                                        ? {uri: profileImagesList[getUid()]}
                                        : require('src/assets/images/user_placeholder_image.png')
                                }
                                style={styles.userProfileStyle}
                            />
                        )}
                        <View style={styles.vwDetails}>
                            <View style={styles.vwNameAndTime}>
                                <Label
                                    numberOfLines={1}
                                    ellipsizeMode="tail"
                                    style={styles.nameStyle}
                                    color={Color.BLACK}>
                                    {getName()}
                                </Label>

                                <Label
                                    style={styles.tagStyle}
                                    xsmall
                                    color={Color.DARK_LIGHT_BLACK}>
                                    {getTag(itemData.timestamp)}
                                </Label>
                            </View>
                            <Label
                                small
                                numberOfLines={1}
                                ellipsizeMode="tail"
                                color={Color.BLACK}>
                                {itemData.content}
                            </Label>
                        </View>
                    </Ripple>
                )}
            </>
        );
    };

    return (
        <>
            {newGroupModalVisible && (
                <View>
                    <Modal
                        animationType="slide"
                        transparent={false}
                        visible={newGroupModalVisible}
                        onRequestClose={() => setNewGroupModalVisible(false)}>
                        <View style={CommonStyle.master_full_flex}>
                            <View style={styles.serchContainer}>
                                <View style={styles.vwInput}>
                                    <MaterialTextInput
                                        value={searchUser}
                                        style={styles.inputStyle}
                                        onChangeText={(text) =>
                                            searchFilterFunction(text)
                                        }
                                        placeholder={Strings.search}
                                        lineWidth={0}
                                        activeLineWidth={0}
                                        disabledLineWidth={0}
                                        placeholderTextColor={
                                            Color.TEXT_PLACEHOLDER
                                        }
                                    />
                                </View>
                                <Ripple
                                    rippleContainerBorderRadius={50}
                                    onPress={() => handleClose()}
                                    style={styles.searchIconStyle}>
                                    <IonIcon
                                        name="close-outline"
                                        style={styles.searchIcon}
                                        color={Color.BLACK}
                                        size={25}
                                    />
                                </Ripple>
                            </View>
                            <View style={styles.vwSelectedUsers}>
                                {selectedUsers?.length > 0 && (
                                    <FlatList
                                        data={selectedUsers}
                                        horizontal
                                        showsHorizontalScrollIndicator={false}
                                        renderItem={({item}) => (
                                            <View
                                                style={styles.vwUser}
                                                key={item?.uid}>
                                                <Image
                                                    source={
                                                        item?.imageUrl
                                                            ? {
                                                                  uri: item?.imageUrl,
                                                              }
                                                            : require('src/assets/images/user_placeholder_image.png')
                                                    }
                                                    style={
                                                        styles.userProfileStyle
                                                    }
                                                />
                                                <Ripple
                                                    rippleContainerBorderRadius={
                                                        50
                                                    }
                                                    onPress={() =>
                                                        handleCloseUser(
                                                            item?.uid,
                                                        )
                                                    }
                                                    style={
                                                        styles.closeIconStyle
                                                    }>
                                                    <IonIcon
                                                        name="close-outline"
                                                        color={Color.BLACK}
                                                        size={20}
                                                    />
                                                </Ripple>
                                            </View>
                                        )}
                                        keyExtractor={(item) =>
                                            item?.uid.toString()
                                        }
                                    />
                                )}
                            </View>
                            <FlatList
                                style={styles.allUserListStyle}
                                data={allUserList}
                                renderItem={({item}) => (
                                    <RenderItemCompo itemData={item} />
                                )}
                                keyExtractor={(item) => item.uid.toString()}
                            />
                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => goToCreateGroup()}
                                style={styles.newGroupIconStyle}>
                                <IonIcon
                                    name="arrow-forward-outline"
                                    style={styles.vwGroupChat}
                                    color={Color.WHITE}
                                    size={30}
                                />
                            </Ripple>
                        </View>
                    </Modal>
                </View>
            )}
            <View style={CommonStyle.master_full_flex}>
                <View style={styles.container}>
                    {/* <Label bolder onPress={handlePushNotification}>{'Push Notification'}</Label> */}
                    {visible ? (
                        <ActivityIndicator
                            animating={true}
                            color={Color.PRIMARY_DARK}
                            size={'large'}
                        />
                    ) : (
                        <>
                            <FlatList
                                data={list}
                                renderItem={({item}) => (
                                    <RenderItemCompo itemData={item} />
                                )}
                                keyExtractor={(item) =>
                                    item?.chatKey?.toString()
                                }
                            />

                            <Ripple
                                rippleContainerBorderRadius={50}
                                onPress={() => handleNewGroup()}
                                style={styles.newGroupIconStyle}>
                                <IonIcon
                                    name="people-outline"
                                    style={styles.vwGroupChat}
                                    color={Color.WHITE}
                                    size={ThemeUtils.relativeWidth(8)}
                                />
                            </Ripple>
                        </>
                    )}
                    {/* <View style={styles.outerView1}>
                        <View style={styles.view1}>
                            <Label>{'View1'}</Label>
                        </View>
                    </View>
                    <View style={styles.outerView2}>
                        <View style={styles.view2}>
                            <Label>{'View2'}</Label>
                        </View>
                    </View> */}
                </View>
            </View>
        </>
    );
};

export default Home;
