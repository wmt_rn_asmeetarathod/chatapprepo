import {StyleSheet} from 'react-native';
import {Color, ThemeUtils} from 'src/utils';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        flexDirection: 'column',
    },
    outerView1: {
        backgroundColor: Color.WHITE,
    },
    view1: {
        height: ThemeUtils.relativeHeight(40),
        backgroundColor: '#181D3D',
        borderBottomLeftRadius: 90,
        padding: 20,
    },
    outerView2: {
        backgroundColor: '#181D3D',
    },
    view2: {
        height: ThemeUtils.relativeHeight(40),
        backgroundColor: Color.WHITE,
        borderTopRightRadius: 90,
        padding: 20,
    },
    msgStyle: {
        backgroundColor: Color.PRIMARY_DARK,
        color: Color.WHITE,
        borderRadius: 10,
    },
    rplStyle: {
        backgroundColor: Color.WHITE,
        padding: 17,
        borderBottomColor: Color.TEXT_PLACEHOLDER,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    selectedStyle: {
        backgroundColor: Color.MANDARIAN_ORANGE,
        padding: 17,
        borderBottomColor: Color.TEXT_PLACEHOLDER,
        borderBottomWidth: 0.5,
        flexDirection: 'row',
        alignItems: 'center',
    },
    vwDetails: {
        flex: 1,
    },
    nameStyle: {
        flex: 1,
        marginEnd: 65,
    },
    tagStyle: {
        position: 'absolute',
        top: 0,
        right: 0,
    },
    vwNameAndTime: {
        flexDirection: 'row',
    },
    headerButtonsStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginEnd: 10,
    },
    searchIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },

    searchIcon: {
        marginStart: 10,
        marginEnd: 10,
    },
    serchContainer: {
        flexDirection: 'row',
        padding: 8,
        justifyContent: 'center',
        alignItems: 'center',
        height: ThemeUtils.APPBAR_HEIGHT,
        borderColor: Color.LIGHT_GRAY,
        borderWidth: 1,
        borderRadius: 30,
        margin: 5,
    },
    vwInput: {
        flex: 1,
        height: ThemeUtils.APPBAR_HEIGHT,
        justifyContent: 'center',
    },
    inputStyle: {
        alignSelf: 'center',
        paddingStart: 10,
    },
    userProfileStyle: {
        width: ThemeUtils.relativeWidth(15),
        height: ThemeUtils.relativeWidth(15),
        alignSelf: 'center',
        borderRadius: ThemeUtils.relativeWidth(7.5),
        marginEnd: ThemeUtils.relativeWidth(4),
    },
    allUserListStyle: {
        // marginBottom : 90,
    },
    newGroupIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: 20,
        bottom: 20,
        backgroundColor: Color.PRIMARY_DARK,
        borderRadius: 100,
        padding: ThemeUtils.relativeWidth(3),
        aspectRatio: 1,
    },
    vwGroupChat: {},
    vwSelectedUsers: {
        // backgroundColor: 'green',
        // height: 100,
        flexDirection: 'row',
    },
    vwUser: {
        padding: 2,
    },
    closeIconStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        bottom: 0,
        right: 20,
        backgroundColor: Color.TEXT_PLACEHOLDER,
        borderRadius: 50,
    },
});
