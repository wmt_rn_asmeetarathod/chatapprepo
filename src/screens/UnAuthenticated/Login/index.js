/* eslint-disable  */
import React, {useEffect, useState} from 'react';
import {ToastAndroid, View, TouchableOpacity} from 'react-native';
import {Controller, useForm} from 'react-hook-form';
import {CommonActions} from '@react-navigation/routers';
import auth from '@react-native-firebase/auth';
import {
    GoogleSignin,
    statusCodes,
} from '@react-native-google-signin/google-signin';
import {MaterialTextInput, RoundButton, Label} from 'src/component';
import {
    CommonStyle,
    Constants,
    Messages,
    ThemeUtils,
    Strings,
    Color,
} from 'src/utils';
import styles from './styles';
import Routes from 'src/router/Routes';

const Login = (props) => {
    const [visible, setVisible] = useState(false);
    const {
        register,
        setValue,
        control,
        reset,
        clearErrors,
        getValues,
        errors,
        handleSubmit,
    } = useForm();

    /*  Life-cycles Methods */
    useEffect(() => {
        GoogleSignin.configure({
            offlineAccess: false,
            webClientId: Constants.WebClient.KEY,
        });
        console.log('Configured !');
    }, []);

    /*  UI Events Methods   */

    // Authenticates User for Login
    const onSubmit = (data) => {
        setVisible(true);
        auth()
            .signInWithEmailAndPassword(data.email, data.password)
            .then((res) => {
                setVisible(false);
                console.log(res.user.email);
                props.navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [{name: Routes.Authenticated}],
                    }),
                );
                ToastAndroid.show(Strings.loginSuccessfully, ToastAndroid.LONG);
            })
            .catch((error) => {
                setVisible(false);
                ToastAndroid.show(
                    Strings.notMatchOurRecords,
                    ToastAndroid.LONG,
                );
            });
    };

    const handleLoginWithGoogle = () => {
        loginWithGoogle()
            .then((res) => {
                props.navigation.navigate(Routes.Authenticated);
                ToastAndroid.show(Strings.loginSuccessfully, ToastAndroid.LONG);
            })
            .catch((error) => {
                if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                    ToastAndroid.show(
                        Strings.cancelledLogin,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === statusCodes.IN_PROGRESS) {
                    ToastAndroid.show(
                        Strings.loginInProgress,
                        ToastAndroid.LONG,
                    );
                } else if (
                    error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE
                ) {
                    ToastAndroid.show(
                        Strings.playServiceNotAvailable,
                        ToastAndroid.LONG,
                    );
                } else {
                    ToastAndroid.show(
                        Strings.somethingWentWrong,
                        ToastAndroid.LONG,
                    );
                }
                console.log('error ==>>> ', error);
                console.log('error.code ==>>> ', error.code);
            });
    };

    // Method for Login with Google
    const loginWithGoogle = async () => {
        await GoogleSignin.hasPlayServices();
        const {idToken, user} = await GoogleSignin.signIn();

        const googleCredential = auth.GoogleAuthProvider.credential(idToken);
        return auth().signInWithCredential(googleCredential);
    };

    const goToSignUp = () => {
        props.navigation.navigate(Routes.SignUp);
    };

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <Label
                    style={styles.signUpLabelStyle}
                    xlarge
                    bolder
                    name="loginLabel">
                    {Strings.loginFullCaps}
                </Label>
                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('email')}
                            error={errors.email?.message}
                            value={value}
                            label={'Email'}
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'email'}
                    defaultValue={''}
                    rules={{
                        required: Messages.Errors.emailBlank,
                        pattern: {
                            value: Constants.Regex.PASSWORD,
                            message: Messages.Errors.emailValidity,
                        },
                    }}
                />

                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('password')}
                            error={errors.password?.message}
                            value={value}
                            label={'Password'}
                            secureTextEntry
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'password'}
                    defaultValue={''}
                    rules={{required: Messages.Errors.pwdBlank}}
                />

                <View style={styles.vwBtns}>
                    <RoundButton
                        click={handleSubmit(onSubmit)}
                        width={ThemeUtils.relativeRealWidth(90)}
                        mt={ThemeUtils.relativeRealHeight(4)}
                        textColor={Color.WHITE}
                        visible={visible}
                        style={CommonStyle.full_flex}>
                        {Strings.loginFullCaps}
                    </RoundButton>

                    <RoundButton
                        click={() => handleLoginWithGoogle()}
                        width={ThemeUtils.relativeRealWidth(90)}
                        mt={ThemeUtils.relativeRealHeight(4)}
                        textColor={Color.WHITE}
                        style={[CommonStyle.full_flex, styles.btnStyle]}>
                        {Strings.loginWithGoogle}
                    </RoundButton>

                    <TouchableOpacity onPress={() => goToSignUp()}>
                        <Label
                            style={styles.signUpLabelStyle}
                            small
                            mt={25}
                            name="signupLabel">
                            {Strings.signUpHere}
                        </Label>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

export default Login;
