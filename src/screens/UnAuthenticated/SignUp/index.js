/* eslint-disable  */

import React, {useState, useEffect} from 'react';
import styles from './styles';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import Routes from 'src/router/Routes';
import {CommonActions} from '@react-navigation/routers';
import {BackHandler, ToastAndroid, View} from 'react-native';
import {MaterialTextInput, RoundButton, Label} from 'src/component';
import {Controller, useForm} from 'react-hook-form';
import {
    CommonStyle,
    Constants,
    Messages,
    ThemeUtils,
    Strings,
    Color,
} from 'src/utils';

const SignUp = (props) => {
    const [user, setUser] = useState();
    const [visible, setVisible] = useState(false);

    const {
        register,
        setValue,
        control,
        reset,
        clearErrors,
        getValues,
        errors,
        handleSubmit,
    } = useForm();

    useEffect(() => {
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            () => BackHandler.exitApp(),
        );

        return () => backHandler.remove();
    }, []);

    /*  UI Events Methods   */
    const onSubmit = (data) => {
        setVisible(true);
        auth()
            .createUserWithEmailAndPassword(data.email, data.password)
            .then((res) => {
                setVisible(false);
                addDataToDB(
                    data.firstName,
                    data.lastName,
                    data.email,
                    data.password,
                    res.user.uid,
                );
                console.log('User Account Created and Signed In!');
                ToastAndroid.show(
                    Strings.signUpSuccessfully,
                    ToastAndroid.LONG,
                );
                props.navigation.dispatch(
                    CommonActions.reset({
                        index: 1,
                        routes: [{name: Routes.Authenticated}],
                    }),
                );
            })
            .catch((error) => {
                setVisible(false);
                if (error.code === 'auth/email-already-in-use') {
                    ToastAndroid.show(
                        Strings.emailAlreadyExists,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === 'auth/weak-password') {
                    ToastAndroid.show(
                        Strings.passwordTooWeek,
                        ToastAndroid.LONG,
                    );
                } else if (error.code === 'auth/invalid-email') {
                    ToastAndroid.show(
                        Strings.invalidEmailAddress,
                        ToastAndroid.LONG,
                    );
                } else {
                    ToastAndroid.show(
                        Strings.somethingWentWrong,
                        ToastAndroid.LONG,
                    );
                }
            });
    };

    // Add User Data to Realtime Database
    const addDataToDB = async (
        firstName,
        lastName,
        email,
        password,
        userId,
    ) => {
        try {
            await setUser(auth().currentUser);
            await database().ref(`allUsers/${userId}`).set({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password,
                uid: userId,
                timestamp: Date.now(),
            });
            console.log('Success!');
        } catch (error) {
            console.log('(addDataToDB) error ==>> ', error);
        }
    };

    const backToLogin = () => {
        props.navigation.navigate(Routes.Login);
    };

    return (
        <View style={CommonStyle.master_full_flex}>
            <View style={styles.container}>
                <Label
                    style={styles.signUpLabelStyle}
                    xlarge
                    bolder
                    name="signUpLabel">
                    {Strings.signUpFullCaps}
                </Label>

                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('firstName')}
                            error={errors.firstName?.message}
                            value={value}
                            label={'First Name'}
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'firstName'}
                    defaultValue={''}
                    rules={{
                        required: {
                            value: true,
                            message: Messages.Errors.firstNameBlank,
                        },
                    }}
                />

                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('lastName')}
                            error={errors.lastName?.message}
                            value={value}
                            label={'Last Name'}
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'lastName'}
                    defaultValue={''}
                    rules={{
                        required: {
                            value: true,
                            message: Messages.Errors.lastNameBlank,
                        },
                    }}
                />

                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('email')}
                            error={errors.email?.message}
                            value={value}
                            label={'Email'}
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'email'}
                    defaultValue={''}
                    rules={{
                        required: Messages.Errors.emailBlank,
                        pattern: {
                            value: Constants.Regex.PASSWORD,
                            message: Messages.Errors.emailValidity,
                        },
                    }}
                />

                <Controller
                    control={control}
                    render={({onChange, onBlur, value}) => (
                        <MaterialTextInput
                            onFocus={() => clearErrors('password')}
                            error={errors.password?.message}
                            value={value}
                            label={'Password'}
                            secureTextEntry
                            onBlur={onBlur}
                            onChangeText={onChange}
                        />
                    )}
                    name={'password'}
                    defaultValue={''}
                    rules={{required: Messages.Errors.pwdBlank}}
                />

                <View style={styles.vwBtns}>
                    <RoundButton
                        click={handleSubmit(onSubmit)}
                        width={ThemeUtils.relativeRealWidth(90)}
                        mt={ThemeUtils.relativeRealHeight(4)}
                        textColor={Color.WHITE}
                        visible={visible}
                        style={CommonStyle.full_flex}>
                        {Strings.signUpFullCaps}
                    </RoundButton>

                    <RoundButton
                        click={() => backToLogin()}
                        width={ThemeUtils.relativeRealWidth(90)}
                        mt={ThemeUtils.relativeRealHeight(4)}
                        textColor={Color.WHITE}
                        visible={false}
                        style={CommonStyle.full_flex}>
                        {Strings.backtoLoginTitle}
                    </RoundButton>
                </View>
            </View>
        </View>
    );
};

export default SignUp;
