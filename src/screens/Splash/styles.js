/* eslint-disable  */
import {StyleSheet} from 'react-native';
import {Color} from 'src/utils';

export const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Color.PRIMARY_BG,
        display: 'flex',
    },
    logo: {
        // width: '40%',
        // height: '30%',
        width: 110,
        height:110,
    },
});
