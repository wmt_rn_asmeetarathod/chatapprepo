/* eslint-disable  */
import React, { useEffect } from 'react';
import {
    View,
    Image,
    Linking
} from 'react-native';
import { styles } from './styles';
import { IS_IOS } from 'src/utils';
import Routes from 'src/router/Routes';
import { CommonActions } from '@react-navigation/native';
import { connect } from 'react-redux';
import auth from '@react-native-firebase/auth';


const Splash = (props) => {
    const user = auth().currentUser;
    //navigate to authenticated route
    const resetToAuth = CommonActions.reset({
        index: 0,
        routes: [{ name: Routes.Authenticated }],
    });

    //navigate to authenticated route
    const resetToNotAuth = CommonActions.reset({
        index: 0,
        routes: [{ name: Routes.UnAuthenticated }],
    });

    /*  Life-cycles Methods */

    useEffect(() => {
        let splashDelay = IS_IOS ? 100 : 1000;


        if (!user) {
            setTimeout(() => {
                props.navigation.dispatch(resetToNotAuth);
            }, splashDelay);
        } else {
            setTimeout(() => {
                props.navigation.dispatch(resetToAuth);
            }, splashDelay);
        }



    }, []);

    return (
        <View style={styles.mainContainer}>

            <Image
                source={require('../../assets/images/bubble_chat_icon.png')}
                height={100}
                width={100}
                style={styles.logo}
            />
        </View>
    );
};

//set store values as props
const mapStateToProps = (state) => {
    return {
        user: state.user,
        token: state.token,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
