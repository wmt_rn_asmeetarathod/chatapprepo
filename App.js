/**
 * Created using React-Native Base
 * https://webmobtech.com
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import {StatusBar} from 'react-native';

// redux
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import {persistor, store} from './src/redux/store';

import {
    NavigationContainer,
    DefaultTheme,
    getStateFromPath,
} from '@react-navigation/native';
import {SafeAreaProvider} from 'react-native-safe-area-context';

// custom component
import {Toast} from 'src/component';

// utils
import {Color} from 'src/utils';
import RootNavigator from 'src/router';

// navigation
import Routes from './src/router/Routes';

const MyAppTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: Color.PRIMARY,
        background: Color.PRIMARY_BACKGROUND,
        card: Color.PRIMARY,
        text: Color.WHITE,
    },
};

const App = () => {
    const linking = {
        prefixes: ['https://webmobtech.com'],
        config: {
            screens: {
                Authenticated: {
                    screens: {
                        Chat: 'chat/:userId/:receiverId',
                        Home: '*',
                    },
                },
            },
        },
        getStateFromPath(path, config) {
            const defaultState = getStateFromPath(path, config);
            // add first page to routes, then you will have a back btn
            const {routes} = defaultState.routes[0].state;
            const firstRouteName = Routes.Home;
            if (
                routes &&
                routes.length === 1 &&
                routes[0].name !== firstRouteName
            ) {
                defaultState.routes[0].state.routes.unshift({
                    name: firstRouteName,
                });
            }
            return defaultState;
        },
    };

    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <SafeAreaProvider>
                    <StatusBar
                        barStyle="default"
                        backgroundColor={Color.PRIMARY_DARK}
                    />
                    <NavigationContainer linking={linking} theme={MyAppTheme}>
                        <RootNavigator />
                    </NavigationContainer>
                </SafeAreaProvider>
                <Toast ref={(ref) => Toast.setRef(ref)} />
            </PersistGate>
        </Provider>
    );
};

export default App;
